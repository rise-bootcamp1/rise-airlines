﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class AirlinesTests
{
    public static IEnumerable<object[]> AirlinesAddData = new List<object[]>
    {
        new object[]
        {
            new List<Airline>()
            {
                new() { Name = "Azure" },
                new() { Name = "Quest" },
                new() { Name = "Celes" }
            },
            true
        },
        new object[]
        {
            new List<Airline>()
            {
                new() { Name = "Azure" },
                new() { Name = "Azure" },
                new() { Name = "Celes" }
            },
            false
        }
    };

    [Theory]
    [MemberData(nameof(AirlinesAddData))]
    public void AirlinesAddTests(List<Airline> newAirlines, bool expected)
    {
        var airlines = new AirlineManager();
        var result = true;
        foreach (var airline in newAirlines)
        {
            try
            {
                airlines.Add(airline);
                result = true;
            }
            catch (DuplicateException)
            {
                result = false;
                break;
            }
        }
        Assert.Equal(expected, result);
    }
}
