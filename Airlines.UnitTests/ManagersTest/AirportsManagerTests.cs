﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class AirportsTests
{
    public static IEnumerable<object[]> AirportsAddTests = new List<object[]>
    {
        new object[]
        {
            new List<Airport>
            {
                new()
                {
                    Id = "JFK",
                    Name = "John F. Kennedy International Airport",
                    City = "New York",
                    Country = "USA",
                },
                new()
                {
                    Id = "LAX",
                    Name = "Los Angeles International Airport",
                    City = "Los Angeles",
                    Country = "USA",
                },
                new()
                {
                    Id = "SFO",
                    Name = "San Francisco International Airport",
                    City = "San Francisco",
                    Country = "USA",
                }
            },
            true
        },
        new object[]
        {
            new List<Airport>
            {
                new()
                {
                    Id = "JFK",
                    Name = "John F. Kennedy International Airport",
                    City = "New York",
                    Country = "United States",
                },
                new()
                {
                    Id = "LAX",
                    Name = "Los Angeles International Airport",
                    City = "Los Angeles",
                    Country = "United States",
                },
                new()
                {
                    Id = "JFK",
                    Name = "John F. Kennedy International Airport",
                    City = "Washington",
                    Country = "United States",
                }
            },
            false
        }
    };

    [Theory]
    [MemberData(nameof(AirportsAddTests))]
    public void AirportsAddTest(List<Airport> newAirports, bool expected)
    {
        var airports = new AirportManager();
        var result = true;
        foreach (var airport in newAirports)
        {
            try
            {
                airports.Add(airport);
                result = true;
            }
            catch (DuplicateException)
            {
                result = false;
                break;
            }
        }
        Assert.Equal(expected, result);
    }
}
