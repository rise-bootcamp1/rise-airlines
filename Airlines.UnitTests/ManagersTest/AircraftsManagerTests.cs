using Airlines.Business;

namespace Airlines.UnitTests;

public class AircraftsTests
{
    [Fact]
    public void AircraftsLoadTest()
    {
        using (var sw = new StreamWriter("AircraftsData.csv"))
        {
            sw.WriteLine("Model,PassengerCapacity,CargoCapacity,Range");
            sw.WriteLine("A123,100,100,-");
            sw.WriteLine("A124,-,-,2000");
            sw.WriteLine("A125,50,50,1500");
        }
        // Arrange
        var aircrafts = new AircraftManager();
        var path = "AircraftsData.csv";
        // Act
        aircrafts.ParseAircrafts(FileReader.ReadFile(path));
        // Assert
        Assert.Equal(3, aircrafts.Aircrafts.Count);
    }
}