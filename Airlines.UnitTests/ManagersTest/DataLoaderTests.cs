using Airlines.Business;

namespace Airlines.UnitTests;

public class DataLoaderTests
{
    [Fact]
    public void LoadAircraftsTests()
    {
        var path = ".\\AircraftsTestData.csv";
        using (var writer = new StreamWriter(path))
        {
            writer.WriteLine("Aircraft Model,Cargo Weight,Cargo Volume,Seats");
            writer.WriteLine("Boeing 747-8F,140000,854.5,-");
            writer.WriteLine("Airbus A320,20000,37.4,150");
            writer.WriteLine("Gulfstream G650,-,-,18");
        }

        var aircrafts = new AircraftManager();

        aircrafts.ParseAircrafts(FileReader.ReadFile(path));

        Assert.True(aircrafts.Aircrafts.ContainsKey("Boeing 747-8F"));
        Assert.True(aircrafts.Aircrafts.ContainsKey("Airbus A320"));
        Assert.True(aircrafts.Aircrafts.ContainsKey("Gulfstream G650"));

        File.Delete(path);
    }
}