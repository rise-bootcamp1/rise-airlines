﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class FlightsTests
{
    public static IEnumerable<object[]> FlightsDataTests = new List<object[]>
    {
        new object[]
        {
            new List<Flight>()
            {
                new()
                {
                    Id = "FL123",
                    DepartureAirport = new Airport() { Id = "JFK" },
                    ArrivalAirport = new Airport() { Id = "LAX" },
                    Aircraft = new CargoAircraft("Boeing 747-8F"),
                },
                new()
                {
                    Id = "FL456",
                    DepartureAirport = new Airport() { Id = "LAX" },
                    ArrivalAirport = new Airport() { Id = "ORD" },
                    Aircraft = new CargoAircraft("Airbus A320"),
                },
                new()
                {
                    Id = "FL202",
                    DepartureAirport = new Airport() { Id = "SFO" },
                    ArrivalAirport = new Airport() { Id = "MIA" },
                    Aircraft = new CargoAircraft("Gulfstream G650"),
                },
            },
            true
        },
        new object[]
        {
            new List<Flight>()
            {
                new()
                {
                    Id = "FL123",
                    DepartureAirport = new Airport() { Id = "JFK" },
                    ArrivalAirport = new Airport() { Id = "LAX" },
                    Aircraft = new CargoAircraft("Boeing 747-8F"),
                },
                new()
                {
                    Id = "FL456",
                    DepartureAirport = new Airport() { Id = "LAX" },
                    ArrivalAirport = new Airport() { Id = "ORD" },
                    Aircraft = new CargoAircraft("Airbus A320"),
                },
                new()
                {
                    Id = "FL123",
                    DepartureAirport = new Airport() { Id = "JFK" },
                    ArrivalAirport = new Airport() { Id = "LAX" },
                    Aircraft = new CargoAircraft("Boeing 747-8F"),
                },
            },
            false
        }
    };

    [Theory]
    [MemberData(nameof(FlightsDataTests))]
    public void TestFlightsInput(List<Flight> newFlights, bool expected)
    {
        var flights = new FlightManager();
        var result = true;
        foreach (var flight in newFlights)
        {
            try
            {
                flights.Add(flight);
                result = true;
            }
            catch (DuplicateException)
            {
                result = false;
                break;
            }
        }
        Assert.Equal(expected, result);
    }
}
