﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class FlightTests
{
    public static IEnumerable<object[]> FlightData = new List<object[]>
    {
        new object[]
        {
            new List<string>() { "FL123", "JFK", "LAX", "Boeing 747-8F"},
            true
        },
        new object[]
        {
            new List<string>() { "FL456", "LAX", "ORD", "Airbus A320"},
            true
        },
        new object[]
        {
            new List<string>() { "FL456!3", "LAX", "ORD", "Airbus A320"},
            false
        },
        new object[] { new List<string>(), false }
    };

    [Theory]
    [MemberData(nameof(FlightData))]
    public void FlightNameTest(List<string> flightProps, bool expected)
    {
        bool result;
        try
        {
            var airline = new Flight()
            {
                Id = flightProps[0],
                DepartureAirport = new Airport() { Id = flightProps[1] },
                ArrivalAirport = new Airport() { Id = flightProps[2] },
                Aircraft = new CargoAircraft(flightProps[3])
            };
            result = true;
        }
        catch (InvalidFlightException)
        {
            result = false;
        }
        catch (ArgumentOutOfRangeException)
        {
            result = false;
        }
        Assert.Equal(expected, result);
    }
}
