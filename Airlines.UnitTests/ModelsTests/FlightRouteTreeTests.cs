using Airlines.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Airlines.UnitTests.DataTests
{
    public class FlightRouteTreeTests
    {
        [Fact]
        public void TreeFindPathTest()
        {
            var tree = new FlightRouteTree() { StartAirport = new Airport { Id = "JFK" } };
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "SFO" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "SEA" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "MIA" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "DFW" } });

            var expected = new List<Flight>()
            {
                new() { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } },
                new() { DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "SFO" } },
                new() { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "MIA" } }
            };

            Assert.Equal(tree.FindPath(new Airport { Id = "MIA" }), expected);
        }

        [Fact]
        public void TreeFindNoPathTest()
        {
            var tree = new FlightRouteTree() { StartAirport = new Airport { Id = "JFK" } };
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "SFO" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "SEA" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "MIA" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "DFW" } });

            _ = Assert.Throws<NotFoundException>(() => tree.FindPath(new Airport { Id = "SOF" }));
        }

        [Fact]
        public void TreeAddInvalidTest()
        {
            var tree = new FlightRouteTree() { StartAirport = new Airport { Id = "JFK" } };
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "SFO" } });
            _ = Assert.Throws<InvalidFlightException>(()
                => tree.Add(new Flight
                {
                    DepartureAirport = new Airport { Id = "DFW" },
                    ArrivalAirport = new Airport() { Id = "SEA" }
                }));
        }

        [Fact]
        public void TreeAddDuplicateTest()
        {
            var tree = new FlightRouteTree() { StartAirport = new Airport { Id = "JFK" } };
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "SFO" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "SEA" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "SFO" }, ArrivalAirport = new Airport() { Id = "MIA" } });
            tree.Add(new Flight { DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "DFW" } });
            _ = Assert.Throws<InvalidFlightException>(()
                => tree.Add(new Flight
                {
                    DepartureAirport = new Airport { Id = "SFO" },
                    ArrivalAirport = new Airport() { Id = "MIA" }
                }));
        }

        // [Fact]
        // public void TreeLoadFromFileValidTest()
        // {
        //     try
        //     {
        //         var path = "AircraftsTestData.csv";
        //         using (var writer = new StreamWriter(path))
        //         {
        //             writer.WriteLine("DFW");
        //             writer.WriteLine("FL505");
        //             writer.WriteLine("FL123");
        //             writer.WriteLine("FL456");
        //             writer.WriteLine("FL789");
        //         }

        //         var airportsManager = new AirportManager();
        //         airportsManager.Add(new Airport { Id = "DFW" });
        //         airportsManager.Add(new Airport { Id = "JFK" });
        //         airportsManager.Add(new Airport { Id = "LAX" });
        //         airportsManager.Add(new Airport { Id = "ORD" });
        //         airportsManager.Add(new Airport { Id = "ATL" });

        //         var flightsManager = new FlightManager();
        //         flightsManager.Add(new Flight { Id = "FL505", DepartureAirport = new Airport { Id = "DFW" }, ArrivalAirport = new Airport() { Id = "JFK" } });
        //         flightsManager.Add(new Flight { Id = "FL123", DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } });
        //         flightsManager.Add(new Flight { Id = "FL456", DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "ORD" } });
        //         flightsManager.Add(new Flight { Id = "FL789", DepartureAirport = new Airport { Id = "ORD" }, ArrivalAirport = new Airport() { Id = "ATL" } });

        //         var tree = new FlightRouteTree() { StartAirport = new Airport { Id = "JFK" } };
        //         tree.LoadFromFile(path, airportsManager, flightsManager);

        //         File.Delete(path);
        //     }
        //     catch (Exception)
        //     {
        //         Assert.True(false);
        //     }
        // }

        [Fact]
        public void TreeLoadFromFileInvalidTest()
        {
            try
            {
                var path = "AircraftsTestData.csv";
                using (var writer = new StreamWriter(path))
                {
                    writer.WriteLine("DFW");
                    writer.WriteLine("FL505");
                    writer.WriteLine("FL123");
                    writer.WriteLine("FL456");
                    writer.WriteLine("FL789");
                    writer.WriteLine("FL303");
                }

                var airportsManager = new AirportManager();
                airportsManager.Add(new Airport { Id = "DFW" });
                airportsManager.Add(new Airport { Id = "JFK" });
                airportsManager.Add(new Airport { Id = "LAX" });
                airportsManager.Add(new Airport { Id = "ORD" });
                airportsManager.Add(new Airport { Id = "ATL" });
                airportsManager.Add(new Airport { Id = "BOS" });
                airportsManager.Add(new Airport { Id = "SEA" });

                var flightsManager = new FlightManager();
                flightsManager.Add(new Flight { Id = "FL505", DepartureAirport = new Airport { Id = "DFW" }, ArrivalAirport = new Airport() { Id = "JFK" } });
                flightsManager.Add(new Flight { Id = "FL123", DepartureAirport = new Airport { Id = "JFK" }, ArrivalAirport = new Airport() { Id = "LAX" } });
                flightsManager.Add(new Flight { Id = "FL456", DepartureAirport = new Airport { Id = "LAX" }, ArrivalAirport = new Airport() { Id = "ORD" } });
                flightsManager.Add(new Flight { Id = "FL789", DepartureAirport = new Airport { Id = "ORD" }, ArrivalAirport = new Airport() { Id = "ATL" } });
                flightsManager.Add(new Flight { Id = "FL303", DepartureAirport = new Airport { Id = "BOS" }, ArrivalAirport = new Airport() { Id = "SEA" } });

                var tree = new FlightRouteTree() { StartAirport = new Airport { Id = "JFK" } };
                tree.LoadFromFile(path, airportsManager, flightsManager);

                File.Delete(path);
            }
            catch (Exception)
            {
                Assert.True(true);
            }
        }
    }
}