﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class AirportTests
{
    public static IEnumerable<object[]> AirportData = new List<object[]>
    {
        new object[]
        {
            new List<string>() { "JFK", "John F. Kennedy International Airport", "New York", "USA" },
            true
        },
        new object[]
        {
            new List<string>() { "LAX", "Los Angeles International Airport", "Los Angeles", "USA" },
            true
        },
        new object[]
        {
            new List<string>() { "O1RD", "O'Hare International Airport", "Chicago", "USA" },
            true
        },
        new object[]
        {
            new List<string>() { "F", "FFFFF", "press f", "more f" },
            false
        },
        new object[]
        {
            new List<string>() { "FFFF23", "FFFFF", "press f", "more f" },
            false
        },
        new object[]
        {
            new List<string>() { "", "", "", "" },
            false
        },
    };

    public static IEnumerable<object[]> AirportCsvData = new List<object[]>
    {
        new object[] { "DFW,Fort Worth International Airport,Dallas-Fort Worth,USA", true },
        new object[] { "LAX123,Los Angeles%International Airport,Los Angeles,USA", false },
        new object[] { "ATL,Hartsfield-Jackson International Airport,USA", false }
    };

    [Theory]
    [MemberData(nameof(AirportData))]
    public void AirportTest(List<string> airportProps, bool expected)
    {
        bool result;
        try
        {
            var airport = new Airport() { Id = airportProps[0], Name = airportProps[1], City = airportProps[2], Country = airportProps[3] };
            result = true;
        }
        catch (InvalidAirportException)
        {
            result = false;
        }
        Assert.Equal(expected, result);
    }
}
