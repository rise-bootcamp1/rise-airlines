﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class AirlineTests
{
    public static IEnumerable<object[]> AirlineNameData = new List<object[]>
    {
        new object[] { "Quest", true },
        new object[] { "2sad", true },
        new object[] { "Ordona", false },
        new object[] { "", false }
    };

    [Theory]
    [MemberData(nameof(AirlineNameData))]
    public void AirlineNameTest(string airlineName, bool expected)
    {
        bool result;
        try
        {
            var airline = new Airline() { Name = airlineName };
            result = true;
        }
        catch (InvalidAirlineException)
        {
            result = false;
        }

        Assert.Equal(expected, result);
    }
}
