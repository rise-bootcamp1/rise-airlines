﻿using Airlines.Business;

namespace Airlines.UnitTests;

public class FindPathStrategyTests
{
    private readonly FlightNetwork network;
    public FindPathStrategyTests()
    {
        network = new FlightNetwork();
        network.AddAirport(new Airport { Id = "JFK" });
        network.AddAirport(new Airport { Id = "LAX" });
        network.AddAirport(new Airport { Id = "ORD" });
        network.AddAirport(new Airport { Id = "DFW" });
        network.AddAirport(new Airport { Id = "MIA" });
    }
    [Fact]
    public void CheapPathStrategy()
    {
        network.Strategy = new CheapestPathStrategy();
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "LAX" },
            Price = 400
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "LAX" },
            ArrivalAirport = new Airport { Id = "DFW" },
            Price = 300
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "ORD" },
            ArrivalAirport = new Airport { Id = "DFW" },
            Price = 200
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "ORD" },
            Price = 200
        });

        var expected = new List<Flight>()
        {
            new()
            {
                DepartureAirport = new Airport { Id = "JFK" },
                ArrivalAirport = new Airport { Id = "ORD" },
                Price = 200
            },
            new()
            {
                DepartureAirport = new Airport { Id = "ORD" },
                ArrivalAirport = new Airport { Id = "DFW" },
                Price = 200
            }
        };

        Assert.Equal(network.FindPath(new Airport { Id = "JFK" }, new Airport { Id = "DFW" }), expected);
    }
    [Fact]
    public void ShortPathStrategy()
    {
        network.Strategy = new FastestPathStrategy();
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "LAX" },
            Duration = TimeSpan.FromHours(4.5)
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "LAX" },
            ArrivalAirport = new Airport { Id = "DFW" },
            Duration = TimeSpan.FromHours(1.5)
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "ORD" },
            ArrivalAirport = new Airport { Id = "DFW" },
            Duration = TimeSpan.FromHours(3.5)
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "ORD" },
            Duration = TimeSpan.FromHours(3.7)
        });

        var expected = new List<Flight>()
        {
            new()
            {
                DepartureAirport = new Airport { Id = "JFK" },
                ArrivalAirport = new Airport { Id = "LAX" },
                Duration = TimeSpan.FromHours(4.5)
            },
            new()
            {
                DepartureAirport = new Airport { Id = "LAX" },
                ArrivalAirport = new Airport { Id = "ORD" },
                Duration = TimeSpan.FromHours(1.5)
            }
        };

        Assert.Equal(network.FindPath(new Airport { Id = "JFK" }, new Airport { Id = "DFW" }), expected);
    }

    [Fact]
    public void StopsPathStrategy()
    {
        network.Strategy = new FewestStopsPathStrategy();
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "LAX" },
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "LAX" },
            ArrivalAirport = new Airport { Id = "MIA" },
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "MIA" },
            ArrivalAirport = new Airport { Id = "DFW" },
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "ORD" },
            ArrivalAirport = new Airport { Id = "DFW" },
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "ORD" },
        });

        var expected = new List<Flight>()
        {
            new()
            {
                DepartureAirport = new Airport { Id = "JFK" },
                ArrivalAirport = new Airport { Id = "ORD" },
            },
            new()
            {
                DepartureAirport = new Airport { Id = "ORD" },
                ArrivalAirport = new Airport { Id = "DFW" },
            }
        };

        Assert.Equal(network.FindPath(new Airport { Id = "JFK" }, new Airport { Id = "DFW" }), expected);
    }
    [Fact]
    public void NoPathStrategy()
    {
        network.Strategy = new CheapestPathStrategy();
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "LAX" },
            Price = 400
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "LAX" },
            ArrivalAirport = new Airport { Id = "DFW" },
            Price = 300
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "ORD" },
            ArrivalAirport = new Airport { Id = "DFW" },
            Price = 200
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "ORD" },
            Price = 200
        });

        var expected = new List<Flight>()
        {
            new()
            {
                DepartureAirport = new Airport { Id = "JFK" },
                ArrivalAirport = new Airport { Id = "ORD" },
                Price = 200
            },
            new()
            {
                DepartureAirport = new Airport { Id = "ORD" },
                ArrivalAirport = new Airport { Id = "DFW" },
                Price = 200
            }
        };

        _ = Assert.Throws<NotFoundException>(() => network.FindPath(new Airport { Id = "JFK" }, new Airport { Id = "MIA" }));
    }
}
