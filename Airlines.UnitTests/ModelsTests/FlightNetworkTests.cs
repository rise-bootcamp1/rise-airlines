using Airlines.Business;

namespace Airlines.UnitTests;

public class FlightNetworkTests
{
    public static IEnumerable<object[]> AddData = new List<object[]>
    {
        new object[]
        {
            new Flight()
            {
                DepartureAirport = new Airport { Id = "JFK" },
                ArrivalAirport = new Airport { Id = "LAX" }
            },
            true
        },
        new object[]
        {
            new Flight()
            {
                DepartureAirport = new Airport { Id = "JFK" },
                ArrivalAirport = new Airport { Id = "MIA" }
            },
            false
        }
    };
    public static IEnumerable<object[]> ConnectData = new List<object[]>
    {
        new object[]
        {
            new Airport() { Id = "JFK" },
            new Airport() { Id = "DFW" },
            true
        },
        new object[]
        {
            new Airport() { Id = "JFK" },
            new Airport() { Id = "LAX" },
            true
        },
        new object[]
        {
            new Airport() { Id = "JFK" },
            new Airport() { Id = "MIA" },
            false
        }
    };
    public static IEnumerable<object[]> PathData = new List<object[]>
    {
        new object[]
        {
            new Airport() { Id = "JFK" },
            new Airport() { Id = "DFW" },
            new List<Flight>
            {
                new()
                {
                    DepartureAirport = new Airport { Id = "JFK" },
                    ArrivalAirport = new Airport { Id = "LAX" }
                },
                new()
                {
                    DepartureAirport = new Airport { Id = "LAX" },
                    ArrivalAirport = new Airport { Id = "DFW" }
                }
            }
        },
        new object[]
        {
            new Airport() { Id = "JFK" },
            new Airport() { Id = "LAX" },
            new List<Flight>
            {
                new()
                {
                    DepartureAirport = new Airport { Id = "JFK" },
                    ArrivalAirport = new Airport { Id = "LAX" }
                }
            }
        },
        new object[]
        {
            new Airport() { Id = "JFK" },
            new Airport() { Id = "MIA" },
            new List<Flight>()
        }
    };

    [Theory]
    [MemberData(nameof(AddData))]
    public void FlightNetworkAddFlight(Flight flight, bool expected)
    {
        var network = new FlightNetwork();
        network.AddAirport(new Airport { Id = "JFK" });
        network.AddAirport(new Airport { Id = "LAX" });
        network.AddAirport(new Airport { Id = "ORD" });
        network.AddAirport(new Airport { Id = "DFW" });

        bool result;
        try
        {
            network.AddFlight(flight);
            result = true;
        }
        catch (NotFoundException)
        {
            result = false;
        }

        Assert.Equal(result, expected);
    }

    [Theory]
    [MemberData(nameof(ConnectData))]
    public void FlightNetworkIsConnected(Airport start, Airport end, bool expected)
    {
        var network = new FlightNetwork();
        network.AddAirport(new Airport { Id = "JFK" });
        network.AddAirport(new Airport { Id = "LAX" });
        network.AddAirport(new Airport { Id = "ORD" });
        network.AddAirport(new Airport { Id = "DFW" });
        network.AddAirport(new Airport { Id = "MIA" });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "LAX" }
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "LAX" },
            ArrivalAirport = new Airport { Id = "DFW" }
        });

        Assert.Equal(network.AreAirportsConnected(start, end), expected);
    }
    [Theory]
    [MemberData(nameof(PathData))]
    public void FlightNetworkPath(Airport start, Airport end, List<Flight> expected)
    {
        var network = new FlightNetwork();
        network.AddAirport(new Airport { Id = "JFK" });
        network.AddAirport(new Airport { Id = "LAX" });
        network.AddAirport(new Airport { Id = "ORD" });
        network.AddAirport(new Airport { Id = "DFW" });
        network.AddAirport(new Airport { Id = "MIA" });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "JFK" },
            ArrivalAirport = new Airport { Id = "LAX" }
        });
        network.AddFlight(new Flight
        {
            DepartureAirport = new Airport { Id = "LAX" },
            ArrivalAirport = new Airport { Id = "DFW" }
        });

        try
        {
            Assert.Equal(network.FindPath(start, end), expected);
        }
        catch (NotFoundException)
        {
            Assert.Empty(expected);
        }
    }
}