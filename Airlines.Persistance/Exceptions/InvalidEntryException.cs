namespace Airlines.Persistance;
public class InvalidEntryException(string customMessage) : Exception(customMessage)
{
}