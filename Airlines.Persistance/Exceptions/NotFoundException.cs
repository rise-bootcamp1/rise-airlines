namespace Airlines.Persistance;
public class NotFoundException(string message) : Exception(message)
{
}