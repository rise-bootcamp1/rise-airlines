﻿namespace Airlines.Persistance;

public class DuplicateException(string customMessage) : Exception(customMessage)
{
}
