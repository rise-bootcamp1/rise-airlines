﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Airlines.Persistance;
#pragma warning disable IDE0058 // Expression value is never used
public partial class RiseAirlinesContext(DbContextOptions<RiseAirlinesContext> options) : DbContext(options)
{
    public virtual DbSet<AirlineEntity> Airlines { get; set; }

    public virtual DbSet<AirportEntity> Airports { get; set; }

    public virtual DbSet<FlightEntity> Flights { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

        modelBuilder.Entity<AirlineEntity>(entity =>
        {
            entity.HasIndex(e => e.FleetSize, "IX_FleetSize");

            entity.HasIndex(e => e.Founded, "IX_Founded");

            entity.HasIndex(e => e.Name, "UQ__Airlines__737584F6B25CBA78").IsUnique();

            entity.Property(e => e.Description).HasMaxLength(500);
            entity.Property(e => e.Name)
                .HasMaxLength(10)
                .IsUnicode(false)
                .IsFixedLength();
        });

        modelBuilder.Entity<AirportEntity>(entity =>
        {
            entity.HasIndex(e => e.City, "IX_City");

            entity.HasIndex(e => e.Country, "IX_Country");

            entity.HasIndex(e => e.Code, "UQ__Airports__A25C5AA7685E2DA5").IsUnique();

            entity.Property(e => e.City).HasMaxLength(50);
            entity.Property(e => e.Code)
                .HasMaxLength(3)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Country).HasMaxLength(20);
            entity.Property(e => e.Name).HasMaxLength(50);
            entity.Property(e => e.RunwaysCount)
                .HasDefaultValue(0)
                .HasColumnName("Runways_Count");
        });

        modelBuilder.Entity<FlightEntity>(entity =>
        {
            entity.HasIndex(e => e.Arrival, "IX_Arrival");

            entity.HasIndex(e => e.ArrivalDateTime, "IX_ArrivalDateTime");

            entity.HasIndex(e => e.Departure, "IX_Departure");

            entity.HasIndex(e => e.DepartureDateTime, "IX_DepartureDateTime");

            entity.HasIndex(e => e.Code, "UQ__Flights__A25C5AA7C1009084").IsUnique();

            entity.Property(e => e.ArrivalDateTime).HasColumnType("datetime");
            entity.Property(e => e.Code)
                .HasMaxLength(5)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.DepartureDateTime).HasColumnType("datetime");

            entity.HasOne(d => d.ArrivalNavigation).WithMany(p => p.FlightArrivalNavigations)
                .HasForeignKey(d => d.Arrival)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Arrival_Airport");

            entity.HasOne(d => d.DepartureNavigation).WithMany(p => p.FlightDepartureNavigations)
                .HasForeignKey(d => d.Departure)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Departure_Airport");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
#pragma warning restore IDE0058 // Expression value is never used
