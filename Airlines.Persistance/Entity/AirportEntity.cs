﻿namespace Airlines.Persistance;

public partial class AirportEntity
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public string? Name { get; set; }

    public string? Country { get; set; }

    public string? City { get; set; }

    public int? RunwaysCount { get; set; }

    public DateOnly Founded { get; set; }

    public virtual ICollection<FlightEntity> FlightArrivalNavigations { get; set; } = new List<FlightEntity>();

    public virtual ICollection<FlightEntity> FlightDepartureNavigations { get; set; } = new List<FlightEntity>();
}
