﻿namespace Airlines.Persistance;

public partial class AirlineEntity
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public DateOnly Founded { get; set; }

    public int FleetSize { get; set; }

    public string? Description { get; set; }
}
