﻿namespace Airlines.Persistance;

public partial class FlightEntity
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public int Departure { get; set; }

    public int Arrival { get; set; }

    public DateTime DepartureDateTime { get; set; }

    public DateTime ArrivalDateTime { get; set; }

    public virtual AirportEntity ArrivalNavigation { get; set; } = null!;

    public virtual AirportEntity DepartureNavigation { get; set; } = null!;
}
