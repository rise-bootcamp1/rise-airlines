﻿namespace Airlines.Persistance;

public interface IAirlineRepository
{
    Task<AirlineEntity?> GetByIdAsync(int id);
    Task<List<AirlineEntity>> GetByNameAsync(string name);
    Task<List<AirlineEntity>> GetByFleetSizeAsync(int fleetSize);
    Task<List<AirlineEntity>> GetByFoundedDateAsync(DateOnly foundedDate);
    Task<List<AirlineEntity>> GetAllAsync();
    Task<AirlineEntity?> AddAsync(AirlineEntity entity);
    Task<AirlineEntity?> UpdateAsync(AirlineEntity entity);
    Task<AirlineEntity?> DeleteAsync(int id);
    Task<int> GetCountAsync();
    Task<List<AirlineEntity>> GetPageAsync(int pageIndex, int pageSize);
    Task<List<AirlineEntity>> GetByNamePageAsync(int pageIndex, int pageSize, string name);
    Task<List<AirlineEntity>> GetFilteredByFleetPageAsync(int pageIndex, int pageSize, int fleetSize);
    Task<List<AirlineEntity>> GetFilteredByFoundedPageAsync(int pageIndex, int pageSize, DateOnly foundedDate);
}
