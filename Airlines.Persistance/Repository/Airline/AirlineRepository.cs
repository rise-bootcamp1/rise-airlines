﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance;
#pragma warning disable IDE0305 // Simplify collection initialization
public class AirlineRepository(RiseAirlinesContext context) : IAirlineRepository
{
    private readonly RiseAirlinesContext context = context;
    public async Task<AirlineEntity?> AddAsync(AirlineEntity entity)
    {
        try
        {
            _ = await context.Airlines.AddAsync(entity);

            return await context.SaveChangesAsync() > 0 ? entity : null;
        }
        catch (SqlException)
        {
            throw new InvalidEntryException("Invalid airline entry");
        }
    }
    public async Task<AirlineEntity?> DeleteAsync(int id)
    {
        var airline = await GetByIdAsync(id) ?? throw new NotFoundException($"Airline with id {id} not found");

        _ = context.Airlines.Remove(airline);

        return await context.SaveChangesAsync() > 0 ? airline : null;
    }
    public async Task<List<AirlineEntity>> GetAllAsync()
        => await context.Airlines.ToListAsync();
    public async Task<AirlineEntity?> GetByIdAsync(int id)
        => await context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
    public async Task<AirlineEntity?> UpdateAsync(AirlineEntity entity)
    {
        try
        {
            _ = context.Airlines.Update(entity);

            return await context.SaveChangesAsync() > 0 ? entity : null;
        }
        catch (SqlException)
        {
            throw new InvalidEntryException("Invalid airline entry");
        }
    }

    public async Task<List<AirlineEntity>> GetByNameAsync(string name)
    {
        var airlines = context.Airlines.Where(a => a.Name.Contains(name));
        return await airlines.ToListAsync();
    }
    public async Task<List<AirlineEntity>> GetByFleetSizeAsync(int fleetSize)
    {
        var airlines = context.Airlines.Where(a => a.FleetSize >= fleetSize);
        return await airlines.ToListAsync();
    }

    public async Task<int> GetCountAsync() => await context.Airlines.CountAsync();
    public async Task<List<AirlineEntity>> GetByFoundedDateAsync(DateOnly foundedDate)
    {
        var airlines = context.Airlines.Where(a => a.Founded >= foundedDate);
        return await airlines.ToListAsync();
    }

    public async Task<List<AirlineEntity>> GetPageAsync(int pageIndex, int pageSize)
    {
        return await context.Airlines
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
    public async Task<List<AirlineEntity>> GetByNamePageAsync(int pageIndex, int pageSize, string name)
    {
        return await context.Airlines
                .Where(a => a.Name.Contains(name))
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }

    public async Task<List<AirlineEntity>> GetFilteredByFleetPageAsync(int pageIndex, int pageSize, int fleetSize)
    {
        return await context.Airlines
                .Where(a => a.FleetSize >= fleetSize)
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }

    public async Task<List<AirlineEntity>> GetFilteredByFoundedPageAsync(int pageIndex, int pageSize, DateOnly foundedDate)
    {
        return await context.Airlines
                .Where(a => a.Founded >= foundedDate)
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
}
#pragma warning restore IDE0305 // Simplify collection initialization
