﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance;
#pragma warning disable IDE0305 // Simplify collection initialization
public class FlightRepository(RiseAirlinesContext context) : IFlightRepository
{
    private readonly RiseAirlinesContext context = context;
    public async Task<FlightEntity?> AddAsync(FlightEntity entity)
    {
        try
        {
            _ = await context.Flights.AddAsync(entity);

            return await context.SaveChangesAsync() > 0 ? entity : null;
        }
        catch (SqlException)
        {
            throw new InvalidEntryException("Invalid flight entry");
        }
    }
    public async Task<FlightEntity?> DeleteAsync(int id)
    {
        var flight = await GetByIdAsync(id) ?? throw new NotFoundException("Flight not found");

        _ = context.Flights.Remove(flight);

        return await context.SaveChangesAsync() > 0 ? flight : null;
    }
    public async Task<List<FlightEntity>> GetAllAsync()
        => await context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .ToListAsync();
    public async Task<FlightEntity?> GetByIdAsync(int id)
        => await context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .FirstOrDefaultAsync(f => f.Id == id);
    public async Task<FlightEntity?> UpdateAsync(FlightEntity entity)
    {
        try
        {
            _ = context.Flights.Update(entity);

            return await context.SaveChangesAsync() > 0 ? entity : null;
        }
        catch (SqlException)
        {
            throw new InvalidEntryException("Invalid flight entry");
        }
    }

    public async Task<FlightEntity?> GetByCodeAsync(string code)
        => await context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .FirstOrDefaultAsync(f => f.Code == code);

    public async Task<List<FlightEntity>> GetByDepartureAirportAsync(AirportEntity entity)
    {
        var flights = context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .Where(f => f.DepartureNavigation.Code == entity.Code);
        return await flights.ToListAsync();
    }
    public async Task<List<FlightEntity>> GetByArrivalAirportAsync(AirportEntity entity)
    {
        var flights = context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .Where(f => f.ArrivalNavigation.Code == entity.Code);
        return await flights.ToListAsync();
    }
    public async Task<List<FlightEntity>> GetByDepartureDateAsync(DateOnly dateOnly)
    {
        var flights = context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .Where(f => f.DepartureDateTime.Date.CompareTo(dateOnly) <= 0);
        return await flights.ToListAsync();
    }
    public async Task<List<FlightEntity>> GetByArrivalDateAsync(DateOnly dateOnly)
    {
        var flights = context.Flights
            .Include(f => f.ArrivalNavigation)
            .Include(f => f.DepartureNavigation)
            .Where(f => f.DepartureDateTime.Date.CompareTo(dateOnly) >= 0);
        return await flights.ToListAsync();
    }

    public async Task<int> GetCountAsync() => await context.Flights.CountAsync();
    public async Task<List<FlightEntity>> GetPageAsync(int pageIndex, int pageSize)
    {
        return await context.Flights
                .Include(f => f.ArrivalNavigation)
                .Include(f => f.DepartureNavigation)
                .OrderBy(a => a.Code)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
    public async Task<List<FlightEntity>> GetByCodePageAsync(int pageIndex, int pageSize, string code)
    {
        return await context.Flights
                .Include(f => f.ArrivalNavigation)
                .Include(f => f.DepartureNavigation)
                .Where(a => a.Code.Contains(code))
                .OrderBy(a => a.Code)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
    public async Task<List<FlightEntity>> GetFilteredByDeparturePageAsync(int pageIndex, int pageSize, string departure)
    {
        return await context.Flights
                .Include(f => f.ArrivalNavigation)
                .Include(f => f.DepartureNavigation)
                .Where(a => a.DepartureNavigation.Code.Contains(departure))
                .OrderBy(a => a.Code)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
    public async Task<List<FlightEntity>> GetFilteredByArrivalPageAsync(int pageIndex, int pageSize, string arrival)
    {
        return await context.Flights
                .Include(f => f.ArrivalNavigation)
                .Include(f => f.DepartureNavigation)
                .Where(a => a.ArrivalNavigation.Code.Contains(arrival))
                .OrderBy(a => a.Code)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
}
#pragma warning restore IDE0305 // Simplify collection initialization
