﻿namespace Airlines.Persistance;

public interface IFlightRepository
{
    Task<FlightEntity?> GetByIdAsync(int id);
    Task<FlightEntity?> GetByCodeAsync(string code);
    Task<List<FlightEntity>> GetByDepartureAirportAsync(AirportEntity entity);
    Task<List<FlightEntity>> GetByArrivalAirportAsync(AirportEntity entity);
    Task<List<FlightEntity>> GetByDepartureDateAsync(DateOnly dateOnly);
    Task<List<FlightEntity>> GetByArrivalDateAsync(DateOnly dateOnly);
    Task<List<FlightEntity>> GetAllAsync();
    Task<FlightEntity?> AddAsync(FlightEntity entity);
    Task<FlightEntity?> UpdateAsync(FlightEntity entity);
    Task<FlightEntity?> DeleteAsync(int id);
    Task<int> GetCountAsync();
    Task<List<FlightEntity>> GetPageAsync(int pageIndex, int pageSize);
    Task<List<FlightEntity>> GetByCodePageAsync(int pageIndex, int pageSize, string code);
    Task<List<FlightEntity>> GetFilteredByDeparturePageAsync(int pageIndex, int pageSize, string departure);
    Task<List<FlightEntity>> GetFilteredByArrivalPageAsync(int pageIndex, int pageSize, string arrival);
}
