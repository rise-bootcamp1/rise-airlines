#pragma warning disable IDE0305 // Simplify collection initialization
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistance;
public class AirportRepository(RiseAirlinesContext context) : IAirportRepository
{
    private readonly RiseAirlinesContext context = context;

    public async Task<AirportEntity?> GetByIdAsync(int id)
        => await context.Airports.FirstOrDefaultAsync(a => a.Id == id);
    public async Task<List<AirportEntity>> GetAllAsync() => await context.Airports.ToListAsync();
    public async Task<AirportEntity?> AddAsync(AirportEntity entity)
    {
        try
        {
            _ = await context.Airports.AddAsync(entity);

            return await context.SaveChangesAsync() > 0 ? entity : null;
        }
        catch (SqlException)
        {
            throw new InvalidEntryException("Invalid airport entry");
        }
    }
    public async Task<AirportEntity?> UpdateAsync(AirportEntity entity)
    {
        try
        {
            _ = context.Airports.Update(entity);

            return await context.SaveChangesAsync() > 0 ? entity : null;
        }
        catch (SqlException)
        {
            throw new InvalidEntryException("Invalid airport entry");
        }
    }
    public async Task<AirportEntity?> DeleteAsync(int id)
    {
        var airport = await GetByIdAsync(id) ?? throw new NotFoundException("Airport not found");

        _ = context.Airports.Remove(airport);

        return await context.SaveChangesAsync() > 0 ? airport : null;
    }

    public async Task<AirportEntity?> GetByCodeAsync(string code)
        => await context.Airports.FirstOrDefaultAsync(a => a.Code == code);
    public async Task<List<AirportEntity>> GetByNameAsync(string name)
    {
        var result = context.Airports.Where(a => a.Name == name);
        return await result.ToListAsync();
    }
    public async Task<List<AirportEntity>> GetByCountryAsync(string country)
    {
        var result = context.Airports.Where(a => a.Country == country);
        return await result.ToListAsync();
    }
    public async Task<List<AirportEntity>> GetByCityAsync(string city)
    {
        var result = context.Airports.Where(a => a.City == city);
        return await result.ToListAsync();
    }
    public async Task<int> GetCountAsync() => await context.Airports.CountAsync();
    public async Task<List<AirportEntity>> GetPageAsync(int pageIndex, int pageSize)
    {
        return await context.Airports
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }

    public async Task<List<AirportEntity>> GetByNamePageAsync(int pageIndex, int pageSize, string name)
    {
        return await context.Airports
                .Where(a => a.Name!.Contains(name))
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
    public async Task<List<AirportEntity>> GetFilteredByCityPageAsync(int pageIndex, int pageSize, string city)
    {
        return await context.Airports
                .Where(a => a.City!.Contains(city))
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
    public async Task<List<AirportEntity>> GetFilteredByCountryPageAsync(int pageIndex, int pageSize, string country)
    {
        return await context.Airports
                .Where(a => a.Country!.Contains(country))
                .OrderBy(a => a.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
    }
}
#pragma warning restore IDE0305 // Simplify collection initialization