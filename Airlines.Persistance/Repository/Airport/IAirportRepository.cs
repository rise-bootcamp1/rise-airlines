﻿namespace Airlines.Persistance;

public interface IAirportRepository
{
    Task<AirportEntity?> GetByIdAsync(int id);
    Task<AirportEntity?> GetByCodeAsync(string code);
    Task<List<AirportEntity>> GetByNameAsync(string name);
    Task<List<AirportEntity>> GetByCountryAsync(string country);
    Task<List<AirportEntity>> GetByCityAsync(string city);
    Task<List<AirportEntity>> GetAllAsync();
    Task<AirportEntity?> AddAsync(AirportEntity entity);
    Task<AirportEntity?> UpdateAsync(AirportEntity entity);
    Task<AirportEntity?> DeleteAsync(int id);
    Task<int> GetCountAsync();
    Task<List<AirportEntity>> GetPageAsync(int pageIndex, int pageSize);
    Task<List<AirportEntity>> GetByNamePageAsync(int pageIndex, int pageSize, string name);
    Task<List<AirportEntity>> GetFilteredByCityPageAsync(int pageIndex, int pageSize, string city);
    Task<List<AirportEntity>> GetFilteredByCountryPageAsync(int pageIndex, int pageSize, string country);
}
