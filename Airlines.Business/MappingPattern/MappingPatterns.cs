using Airlines.Persistance;
using AutoMapper;

namespace Airlines.Business;

public class MappingPatterns : Profile
{
    public MappingPatterns()
    {
        _ = CreateMap<AirlineEntity, AirlineModel>();
        _ = CreateMap<AirlineModel, AirlineEntity>();
        _ = CreateMap<FlightEntity, FlightModel>()
            .ForMember(dest => dest.DepartureAirport, opt => opt.MapFrom(src => src.DepartureNavigation))
            .ForMember(dest => dest.ArrivalAirport, opt => opt.MapFrom(src => src.ArrivalNavigation))
            .ForMember(dest => dest.DepartureTime, opt => opt.MapFrom(src => src.DepartureDateTime))
            .ForMember(dest => dest.ArrivalTime, opt => opt.MapFrom(src => src.ArrivalDateTime));
        _ = CreateMap<FlightModel, FlightEntity>()
            .ForMember(dest => dest.DepartureNavigation, opt => opt.MapFrom(src => src.DepartureAirport))
            .ForMember(dest => dest.ArrivalNavigation, opt => opt.MapFrom(src => src.ArrivalAirport))
            .ForMember(dest => dest.DepartureDateTime, opt => opt.MapFrom(src => src.DepartureTime))
            .ForMember(dest => dest.ArrivalDateTime, opt => opt.MapFrom(src => src.ArrivalTime));
        _ = CreateMap<AirportEntity, AirportModel>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name ?? string.Empty))
            .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country ?? string.Empty))
            .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City ?? string.Empty))
            .ForMember(dest => dest.RunwaysCount, opt => opt.MapFrom(src => src.RunwaysCount ?? 0));
        _ = CreateMap<AirportModel, AirportEntity>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name ?? string.Empty))
            .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country ?? string.Empty))
            .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City ?? string.Empty))
            .ForMember(dest => dest.RunwaysCount, opt => opt.MapFrom(src => src.RunwaysCount));
    }
}