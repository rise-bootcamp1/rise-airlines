﻿using System.Collections.ObjectModel;
using System.Globalization;

namespace Airlines.Business;

public partial class FlightManager
{
    private Dictionary<string, Flight> flights;
    public ReadOnlyDictionary<string, Flight> Flights => new(flights);
    public FlightManager() => flights = [];
    public void Add(Flight flight)
    {
        if (Flights.ContainsKey(flight.Id))
            throw new DuplicateException("Flight with the same ID already exists");

        flights.Add(flight.Id, flight);
    }
    public void Sort(SortType type = SortType.Ascending)
    {
        var sortedDict = type switch
        {
            SortType.Ascending => Flights.OrderBy(x => x.Value.Id),
            SortType.Descending => Flights.OrderByDescending(x => x.Value.Id),
            _ => throw new ArgumentException("Invalid sort type")
        };

        flights = sortedDict.ToDictionary(x => x.Key, x => x.Value);
    }
    public void ParseFlights(List<string> flights, AirportManager airportManager, AircraftManager aircraftManager)
    {
        foreach (var line in flights)
        {
            var parts = line.Trim().Split(',');
            if (parts.Length != 6)
                throw new InvalidInputException("Invalid flight format");

            Add(new Flight
            {
                Id = parts[0],
                DepartureAirport = airportManager.Airports[parts[1]],
                ArrivalAirport = airportManager.Airports[parts[2]],
                Price = decimal.Parse(parts[3], CultureInfo.InvariantCulture),
                Duration = TimeSpan.FromHours(double.Parse(parts[4], CultureInfo.InvariantCulture)),
                Aircraft = aircraftManager.Aircrafts[parts[5]]
            });
        }
    }
}
