﻿using System.Collections.ObjectModel;

namespace Airlines.Business;

public class AirlineManager
{
    private Dictionary<string, Airline> airlines;
    public AirlineManager() => airlines = [];
    public ReadOnlyDictionary<string, Airline> Airlines => new(airlines);
    public void Add(Airline airline)
    {
        if (airlines.ContainsKey(airline.Name))
            throw new DuplicateException("Airline with the same name already exists");

        airlines.Add(airline.Name, airline);
    }
    public void Sort(SortType type = SortType.Ascending)
    {
        var sortedDict = type switch
        {
            SortType.Ascending => airlines.OrderBy(x => x.Value.Name),
            SortType.Descending => airlines.OrderByDescending(x => x.Value.Name),
            _ => throw new ArgumentException("Invalid sort type")
        };

        airlines = sortedDict.ToDictionary(x => x.Key, x => x.Value);
    }
    public void ParseAirlines(List<string> airlines)
    {
        foreach (var line in airlines)
            Add(new Airline { Name = line });
    }
}
