﻿namespace Airlines.Business;

public class FileReader
{
    public static List<string> ReadFile(string filePath)
    {
        var data = new List<string>();
        using (var reader = new StreamReader(filePath))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!string.IsNullOrEmpty(line))
                    data.Add(line);
            }
        }
        data.RemoveAt(0);
        return data;
    }
}
