﻿using System.Collections.ObjectModel;

namespace Airlines.Business;

public class AirportManager
{
    private Dictionary<string, Airport> airports;
    private readonly Dictionary<string, List<Airport>> airportsByCity;
    private readonly Dictionary<string, List<Airport>> airportsByCountry;
    public AirportManager()
    {
        airports = [];
        airportsByCity = [];
        airportsByCountry = [];
    }
    public ReadOnlyDictionary<string, Airport> Airports => new(airports);
    public void Add(Airport airport)
    {
        if (airports.ContainsKey(airport.Id))
            throw new DuplicateException("Airport with the same ID already exists");

        airports.Add(airport.Id, airport);

        if (!airportsByCity.ContainsKey(airport.City))
        {
            airportsByCity.Add(airport.City, []);
            airportsByCity[airport.City].Add(airport);
        }
        else
            airportsByCity[airport.City].Add(airport);

        if (!airportsByCountry.ContainsKey(airport.Country))
        {
            airportsByCountry.Add(airport.Country, []);
            airportsByCountry[airport.Country].Add(airport);
        }
        else
            airportsByCountry[airport.Country].Add(airport);
    }
    public List<Airport> GetByCity(string city)
    {
        if (!airportsByCity.ContainsKey(city))
            throw new NotFoundException("City not found");

        return airportsByCity[city];
    }
    public List<Airport> GetByCountry(string country)
    {
        if (!airportsByCountry.ContainsKey(country))
            throw new NotFoundException("Country not found");

        return airportsByCountry[country];
    }
    public void Sort(SortType type = SortType.Ascending)
    {
        var sortedDict = type switch
        {
            SortType.Ascending => airports.OrderBy(x => x.Value.Id),
            SortType.Descending => airports.OrderByDescending(x => x.Value.Id),
            _ => throw new ArgumentException("Invalid sort type")
        };

        airports = sortedDict.ToDictionary(x => x.Key, x => x.Value);
    }
    public void ParseAirports(List<string> airports)
    {
        foreach (var line in airports)
        {
            var parts = line.Trim().Split(',');
            if (parts.Length != 4)
                throw new InvalidInputException("Invalid airport format");

            Add(new Airport
            {
                Id = parts[0],
                Name = parts[1],
                City = parts[2],
                Country = parts[3]
            });
        }
    }
}
