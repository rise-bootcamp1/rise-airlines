﻿using System.Collections.ObjectModel;
using System.Globalization;

namespace Airlines.Business;

public class AircraftManager
{
    private readonly Dictionary<string, Aircraft> aircrafts;
    public AircraftManager() => aircrafts = [];
    public ReadOnlyDictionary<string, Aircraft> Aircrafts => new(aircrafts);
    public void Add(Aircraft aircraft)
    {
        if (aircrafts.ContainsKey(aircraft.Model))
            throw new DuplicateException("Aircraft with the same model already exists");

        aircrafts.Add(aircraft.Model, aircraft);
    }
    public void ParseAircrafts(List<string> aircrafts)
    {
        foreach (var line in aircrafts)
        {
            var parts = line.Trim().Split(',');
            if (parts.Length != 4)
                throw new InvalidInputException("Invalid aircraft format");

#pragma warning disable IDE0045 // Convert to conditional expression
            if (parts[3] == "-")
            {
                Add(new CargoAircraft(parts[0])
                {
                    CargoWeight = int.Parse(parts[1]),
                    CargoVolume = decimal.Parse(parts[2], CultureInfo.InvariantCulture)
                });
            }
            else if (parts[1] == "-" && parts[2] == "-")
            {
                Add(new PrivateAircraft(parts[0])
                {
                    Seats = int.Parse(parts[3])
                });
            }
            else
            {
                Add(new PassengerAircraft(parts[0])
                {
                    CargoWeight = int.Parse(parts[1]),
                    CargoVolume = decimal.Parse(parts[2], CultureInfo.InvariantCulture),
                    Seats = int.Parse(parts[3])
                });
            }
#pragma warning restore IDE0045 // Convert to conditional expression
        }
    }
}
