﻿using Airlines.Persistance;
using AutoMapper;

namespace Airlines.Business;

public class AirlinesService(IAirlineRepository repository, IMapper mapper) : IAirlinesService
{
    private readonly IAirlineRepository repository = repository;
    private readonly IMapper mapper = mapper;
    public async Task<Result<AirlineModel>> AddAirlineAsync(AirlineModel airline)
    {
        try
        {
            var addedAirline = await repository.AddAsync(mapper.Map<AirlineEntity>(airline));
            return addedAirline != null ?
                   Result<AirlineModel>.Success(mapper.Map<AirlineModel>(addedAirline)) :
                   Result<AirlineModel>.Failure("Could not add airline");
        }
        catch (Exception e)
        {
            return Result<AirlineModel>.Failure(e.Message);
        }
    }
    public async Task<Result<AirlineModel>> DeleteAirlineAsync(int id)
    {
        try
        {
            var deletedAirline = await repository.DeleteAsync(id);
            return deletedAirline != null ?
                   Result<AirlineModel>.Success(mapper.Map<AirlineModel>(deletedAirline)) :
                   Result<AirlineModel>.Failure("Could not delete airline");
        }
        catch (Exception e)
        {
            return Result<AirlineModel>.Failure(e.Message);
        }
    }
    public async Task<Result<List<AirlineModel>>> GetAirlinesAsync()
    {
        try
        {
            var airlines = await repository.GetAllAsync();
            return Result<List<AirlineModel>>.Success(mapper.Map<List<AirlineModel>>(airlines));
        }
        catch (Exception e)
        {
            return Result<List<AirlineModel>>.Failure(e.Message);
        }
    }
    public async Task<Result<AirlineModel>> GetAirlineByIdAsync(int id)
    {
        try
        {
            var airline = await repository.GetByIdAsync(id);

            return airline != null ?
                   Result<AirlineModel>.Success(mapper.Map<AirlineModel>(airline)) :
                   Result<AirlineModel>.Failure("Could not get airline");
        }
        catch (Exception e)
        {
            return Result<AirlineModel>.Failure(e.Message);
        }
    }

    public async Task<Result<AirlineModel>> UpdateAirlineAsync(AirlineModel airline)
    {
        try
        {
            var updatedAirline = await repository.UpdateAsync(mapper.Map<AirlineEntity>(airline));
            return updatedAirline != null ?
                   Result<AirlineModel>.Success(mapper.Map<AirlineModel>(updatedAirline)) :
                   Result<AirlineModel>.Failure("Could not delete airline");
        }
        catch (Exception e)
        {
            return Result<AirlineModel>.Failure(e.Message);
        }
    }
    public async Task<List<AirlineModel>> GetAirlinesByNameAsync(string name)
    {
        var airlines = await repository.GetByNameAsync(name);
        return mapper.Map<List<AirlineModel>>(airlines);
    }
    public async Task<List<AirlineModel>> GetAirlinesByFleetSizeAsync(int fleetSize)
    {
        var airlines = await repository.GetByFleetSizeAsync(fleetSize);
        return mapper.Map<List<AirlineModel>>(airlines);
    }
    public async Task<List<AirlineModel>> GetAirlinesByFoundedDateAsync(DateOnly foundedDate)
    {
        var airlines = await repository.GetByFoundedDateAsync(foundedDate);
        return mapper.Map<List<AirlineModel>>(airlines);
    }
    public async Task<int> GetAirlinesCountAsync() => await repository.GetCountAsync();
    public async Task<List<AirlineModel>> GetAirlinesPageAsync(int pageIndex, int pageSize)
    {
        var airlines = await repository.GetPageAsync(pageIndex, pageSize);
        return mapper.Map<List<AirlineModel>>(airlines);
    }

    public async Task<List<AirlineModel>> GetAirlinesByNamePageAsync(int pageIndex, int pageSize, string name)
    {
        var airlines = await repository.GetByNamePageAsync(pageIndex, pageSize, name);
        return mapper.Map<List<AirlineModel>>(airlines);
    }

    public async Task<List<AirlineModel>> GetAirlinesFilteredByFleetPageAsync(int pageIndex, int pageSize, int fleetSize)
    {
        var airlines = await repository.GetFilteredByFleetPageAsync(pageIndex, pageSize, fleetSize);
        return mapper.Map<List<AirlineModel>>(airlines);
    }

    public async Task<List<AirlineModel>> GetAirlinesFilteredByFoundedPageAsync(int pageIndex, int pageSize, DateOnly foundedDate)
    {
        var airlines = await repository.GetFilteredByFoundedPageAsync(pageIndex, pageSize, foundedDate);
        return mapper.Map<List<AirlineModel>>(airlines);
    }
}
