﻿namespace Airlines.Business;

public interface IAirlinesService
{
    Task<Result<AirlineModel>> AddAirlineAsync(AirlineModel airline);
    Task<Result<AirlineModel>> DeleteAirlineAsync(int id);
    Task<Result<List<AirlineModel>>> GetAirlinesAsync();
    Task<Result<AirlineModel>> GetAirlineByIdAsync(int id);
    Task<Result<AirlineModel>> UpdateAirlineAsync(AirlineModel airline);
    Task<List<AirlineModel>> GetAirlinesByNameAsync(string name);
    Task<List<AirlineModel>> GetAirlinesByFleetSizeAsync(int fleetSize);
    Task<List<AirlineModel>> GetAirlinesByFoundedDateAsync(DateOnly foundedDate);
    Task<int> GetAirlinesCountAsync();
    Task<List<AirlineModel>> GetAirlinesPageAsync(int pageIndex, int pageSize);
    Task<List<AirlineModel>> GetAirlinesByNamePageAsync(int pageIndex, int pageSize, string name);
    Task<List<AirlineModel>> GetAirlinesFilteredByFleetPageAsync(int pageIndex, int pageSize, int fleetSize);
    Task<List<AirlineModel>> GetAirlinesFilteredByFoundedPageAsync(int pageIndex, int pageSize, DateOnly foundedDate);
}
