﻿using Airlines.Persistance;
using AutoMapper;

namespace Airlines.Business;

public class AirportsService(IAirportRepository repository, IMapper mapper) : IAirportsService
{
    private readonly IAirportRepository repository = repository;
    private readonly IMapper mapper = mapper;

    public async Task<Result<AirportModel>> GetAirportByIdAsync(int id)
    {
        try
        {
            var airport = await repository.GetByIdAsync(id);
            return airport != null ?
                   Result<AirportModel>.Success(mapper.Map<AirportModel>(airport)) :
                   Result<AirportModel>.Failure("Could not get airport");
        }
        catch (Exception e)
        {
            return Result<AirportModel>.Failure(e.Message);
        }
    }
    public async Task<Result<List<AirportModel>>> GetAirportsAsync()
    {
        try
        {
            var airports = await repository.GetAllAsync();
            return Result<List<AirportModel>>.Success(mapper.Map<List<AirportModel>>(airports));
        }
        catch (Exception e)
        {
            return Result<List<AirportModel>>.Failure(e.Message);
        }
    }
    public async Task<Result<AirportModel>> AddAirportAsync(AirportModel airport)
    {
        try
        {
            var addedAirport = await repository.AddAsync(mapper.Map<AirportEntity>(airport));
            return addedAirport != null ?
                   Result<AirportModel>.Success(mapper.Map<AirportModel>(addedAirport)) :
                   Result<AirportModel>.Failure("Could not add airport");
        }
        catch (Exception e)
        {
            return Result<AirportModel>.Failure(e.Message);
        }
    }
    public async Task<Result<AirportModel>> UpdateAirportAsync(AirportModel airport)
    {
        try
        {
            var updatedAirport = await repository.UpdateAsync(mapper.Map<AirportEntity>(airport));
            return updatedAirport != null ?
                   Result<AirportModel>.Success(mapper.Map<AirportModel>(updatedAirport)) :
                   Result<AirportModel>.Failure("Could not update airport");
        }
        catch (Exception e)
        {
            return Result<AirportModel>.Failure(e.Message);
        }
    }
    public async Task<Result<AirportModel>> DeleteAirportAsync(int id)
    {
        try
        {
            var deletedAirport = await repository.DeleteAsync(id);
            return deletedAirport != null ?
                   Result<AirportModel>.Success(mapper.Map<AirportModel>(deletedAirport)) :
                   Result<AirportModel>.Failure("Could not delete airport");
        }
        catch (Exception e)
        {
            return Result<AirportModel>.Failure(e.Message);
        }
    }
    public async Task<AirportModel?> GetAirportByCodeAsync(string code)
    {
        var airport = await repository.GetByCodeAsync(code);
        return airport != null ? mapper.Map<AirportModel>(airport) : null;
    }
    public async Task<List<AirportModel>> GetAirportsByNameAsync(string name)
    {
        var airports = await repository.GetByNameAsync(name);
        return mapper.Map<List<AirportModel>>(airports);
    }
    public async Task<List<AirportModel>> GetAirportsByCountryAsync(string country)
    {
        var airports = await repository.GetByCountryAsync(country);
        return mapper.Map<List<AirportModel>>(airports);
    }
    public async Task<List<AirportModel>> GetAirportsByCityAsync(string city)
    {
        var airports = await repository.GetByCityAsync(city);
        return mapper.Map<List<AirportModel>>(airports);
    }
    public async Task<int> GetAirportsCountAsync() => await repository.GetCountAsync();
    public async Task<List<AirportModel>> GetAirportsPageAsync(int pageIndex, int pageSize)
    {
        var airports = await repository.GetPageAsync(pageIndex, pageSize);
        return mapper.Map<List<AirportModel>>(airports);
    }
    public async Task<List<AirportModel>> GetAirportsByNamePageAsync(int pageIndex, int pageSize, string name)
    {
        var airports = await repository.GetByNamePageAsync(pageIndex, pageSize, name);
        return mapper.Map<List<AirportModel>>(airports);
    }
    public async Task<List<AirportModel>> GetAirportsFilteredByCityPageAsync(int pageIndex, int pageSize, string city)
    {
        var airports = await repository.GetFilteredByCityPageAsync(pageIndex, pageSize, city);
        return mapper.Map<List<AirportModel>>(airports);
    }
    public async Task<List<AirportModel>> GetAirportsFilteredByCountryPageAsync(int pageIndex, int pageSize, string country)
    {
        var airports = await repository.GetFilteredByCountryPageAsync(pageIndex, pageSize, country);
        return mapper.Map<List<AirportModel>>(airports);
    }
}
