namespace Airlines.Business;

public interface IAirportsService
{
    Task<Result<AirportModel>> GetAirportByIdAsync(int id);
    Task<Result<List<AirportModel>>> GetAirportsAsync();
    Task<Result<AirportModel>> AddAirportAsync(AirportModel airport);
    Task<Result<AirportModel>> UpdateAirportAsync(AirportModel airport);
    Task<Result<AirportModel>> DeleteAirportAsync(int id);
    Task<AirportModel?> GetAirportByCodeAsync(string code);
    Task<List<AirportModel>> GetAirportsByNameAsync(string name);
    Task<List<AirportModel>> GetAirportsByCountryAsync(string country);
    Task<List<AirportModel>> GetAirportsByCityAsync(string city);
    Task<int> GetAirportsCountAsync();
    Task<List<AirportModel>> GetAirportsPageAsync(int pageIndex, int pageSize);
    Task<List<AirportModel>> GetAirportsByNamePageAsync(int pageIndex, int pageSize, string name);
    Task<List<AirportModel>> GetAirportsFilteredByCityPageAsync(int pageIndex, int pageSize, string city);
    Task<List<AirportModel>> GetAirportsFilteredByCountryPageAsync(int pageIndex, int pageSize, string country);
}
