namespace Airlines.Business;

public interface IFlightsService
{
    Task<Result<FlightModel>> AddFlightAsync(FlightModel flight);
    Task<Result<FlightModel>> DeleteFlightAsync(int id);
    Task<Result<List<FlightModel>>> GetFlightsAsync();
    Task<Result<FlightModel>> GetFlightByIdAsync(int id);
    Task<Result<FlightModel>> UpdateFlightAsync(FlightModel flight);
    Task<FlightModel?> GetFlightByCodeAsync(string code);
    Task<List<FlightModel>> GetFlightsByDepartureAirportAsync(AirportModel airport);
    Task<List<FlightModel>> GetFlightsByArrivalAirportAsync(AirportModel airport);
    Task<List<FlightModel>> GetFlightsByDepartureDateAsync(DateOnly dateOnly);
    Task<List<FlightModel>> GetFlightsByArrivalDateAsync(DateOnly dateOnly);
    Task<int> GetFlightsCountAsync();
    Task<List<FlightModel>> GetFlightsPageAsync(int pageIndex, int pageSize);
    Task<List<FlightModel>> GetFlightsByCodePageAsync(int pageIndex, int pageSize, string code);
    Task<List<FlightModel>> GetFlightsFilteredByDeparturePageAsync(int pageIndex, int pageSize, string departure);
    Task<List<FlightModel>> GetFlightsFilteredByArrivalPageAsync(int pageIndex, int pageSize, string arrival);
}
