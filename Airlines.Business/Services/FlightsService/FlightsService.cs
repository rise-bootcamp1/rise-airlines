﻿using Airlines.Persistance;
using AutoMapper;

namespace Airlines.Business;

public class FlightsService(IFlightRepository flightRepository, IAirportRepository airportRepository, IMapper mapper) : IFlightsService
{
    private readonly IFlightRepository repository = flightRepository;
    private readonly IMapper mapper = mapper;

    public async Task<Result<FlightModel>> AddFlightAsync(FlightModel flight)
    {
        try
        {
            var departure = await airportRepository.GetByCodeAsync(flight.DepartureAirport.Code) ?? throw new NotFoundException("Departure airport not found");
            var arrival = await airportRepository.GetByCodeAsync(flight.ArrivalAirport.Code) ?? throw new NotFoundException("Arrival airport not found");
            var flightEntity = mapper.Map<FlightEntity>(flight);

            flightEntity.Departure = departure.Id;
            flightEntity.Arrival = arrival.Id;
            flightEntity.DepartureNavigation = departure;
            flightEntity.ArrivalNavigation = arrival;

            var addedFlight = await repository.AddAsync(flightEntity);
            return addedFlight != null ?
                   Result<FlightModel>.Success(mapper.Map<FlightModel>(addedFlight)) :
                   Result<FlightModel>.Failure("Could not add flight");
        }
        catch (Exception ex)
        {
            return Result<FlightModel>.Failure(ex.Message);
        }
    }
    public async Task<Result<FlightModel>> DeleteFlightAsync(int id)
    {
        try
        {
            var deletedFlight = await repository.DeleteAsync(id);
            return deletedFlight != null ?
                   Result<FlightModel>.Success(mapper.Map<FlightModel>(deletedFlight)) :
                   Result<FlightModel>.Failure("Could not add flight");
        }
        catch (Exception ex)
        {
            return Result<FlightModel>.Failure(ex.Message);
        }
    }
    public async Task<Result<List<FlightModel>>> GetFlightsAsync()
    {
        try
        {
            var flights = await repository.GetAllAsync();
            return Result<List<FlightModel>>.Success(mapper.Map<List<FlightModel>>(flights));
        }
        catch (Exception ex)
        {
            return Result<List<FlightModel>>.Failure(ex.Message);
        }
    }
    public async Task<Result<FlightModel>> GetFlightByIdAsync(int id)
    {
        try
        {
            var flight = await repository.GetByIdAsync(id);
            return flight != null ?
                   Result<FlightModel>.Success(mapper.Map<FlightModel>(flight)) :
                   Result<FlightModel>.Failure("Could not get flight");
        }
        catch (Exception ex)
        {
            return Result<FlightModel>.Failure(ex.Message);
        }
    }
    public async Task<Result<FlightModel>> UpdateFlightAsync(FlightModel flight)
    {
        try
        {
            var departure = await airportRepository.GetByCodeAsync(flight.DepartureAirport.Code) ?? throw new NotFoundException("Departure airport not found");
            var arrival = await airportRepository.GetByCodeAsync(flight.ArrivalAirport.Code) ?? throw new NotFoundException("Arrival airport not found");
            var flightEntity = mapper.Map<FlightEntity>(flight);

            flightEntity.Departure = departure.Id;
            flightEntity.Arrival = arrival.Id;
            flightEntity.DepartureNavigation = departure;
            flightEntity.ArrivalNavigation = arrival;

            var updatedFlight = await repository.UpdateAsync(flightEntity);
            return updatedFlight != null ?
                   Result<FlightModel>.Success(mapper.Map<FlightModel>(updatedFlight)) :
                   Result<FlightModel>.Failure("Could not update flight");
        }
        catch (Exception ex)
        {
            return Result<FlightModel>.Failure(ex.Message);
        }
    }
    public async Task<FlightModel?> GetFlightByCodeAsync(string code)
    {
        var flight = await repository.GetByCodeAsync(code);
        return flight != null ? mapper.Map<FlightModel>(flight) : null;
    }
    public async Task<List<FlightModel>> GetFlightsByDepartureAirportAsync(AirportModel airport)
    {
        var flights = await repository.GetByDepartureAirportAsync(mapper.Map<AirportEntity>(airport));
        return mapper.Map<List<FlightModel>>(flights);
    }
    public async Task<List<FlightModel>> GetFlightsByArrivalAirportAsync(AirportModel airport)
    {
        var flights = await repository.GetByArrivalAirportAsync(mapper.Map<AirportEntity>(airport));
        return mapper.Map<List<FlightModel>>(flights);
    }
    public async Task<List<FlightModel>> GetFlightsByDepartureDateAsync(DateOnly dateOnly)
    {
        var flights = await repository.GetByDepartureDateAsync(dateOnly);
        return mapper.Map<List<FlightModel>>(flights);
    }
    public async Task<List<FlightModel>> GetFlightsByArrivalDateAsync(DateOnly dateOnly)
    {
        var flights = await repository.GetByArrivalDateAsync(dateOnly);
        return mapper.Map<List<FlightModel>>(flights);
    }

    public async Task<int> GetFlightsCountAsync() => await repository.GetCountAsync();
    public async Task<List<FlightModel>> GetFlightsPageAsync(int pageIndex, int pageSize)
    {
        var flights = await repository.GetPageAsync(pageIndex, pageSize);
        return mapper.Map<List<FlightModel>>(flights);
    }
    public async Task<List<FlightModel>> GetFlightsByCodePageAsync(int pageIndex, int pageSize, string code)
    {
        var flights = await repository.GetByCodePageAsync(pageIndex, pageSize, code);
        return mapper.Map<List<FlightModel>>(flights);
    }
    public async Task<List<FlightModel>> GetFlightsFilteredByDeparturePageAsync(int pageIndex, int pageSize, string departure)
    {
        var flights = await repository.GetFilteredByDeparturePageAsync(pageIndex, pageSize, departure);
        return mapper.Map<List<FlightModel>>(flights);
    }
    public async Task<List<FlightModel>> GetFlightsFilteredByArrivalPageAsync(int pageIndex, int pageSize, string arrival)
    {
        var flights = await repository.GetFilteredByArrivalPageAsync(pageIndex, pageSize, arrival);
        return mapper.Map<List<FlightModel>>(flights);
    }
}
