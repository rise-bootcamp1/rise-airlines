namespace Airlines.Business;
public class Result<T>
{
    public bool IsSuccess { get; set; }
    public T? Value { get; set; }
    // Maybe for logging purposes
    public string? Error { get; set; }

    public static Result<T> Success(T value)
        => new() { IsSuccess = true, Value = value, Error = null };
    public static Result<T> Failure(string error)
        => new() { IsSuccess = true, Error = error, Value = default };
}