﻿namespace Airlines.Business;

public enum SortType
{
    Ascending,
    Descending
}
