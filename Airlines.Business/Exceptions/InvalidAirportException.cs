﻿namespace Airlines.Business;

public class InvalidAirportException(string customMessage) : Exception(customMessage)
{
}
