﻿namespace Airlines.Business;

public class InvalidAirlineException(string customMessage) : Exception(customMessage)
{
}
