using System.Runtime.Serialization;

namespace Airlines.Business;

public class InvalidInputException(string customMessage) : Exception(customMessage)
{
}