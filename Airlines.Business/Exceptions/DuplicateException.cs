﻿namespace Airlines.Business;

public class DuplicateException(string customMessage) : Exception(customMessage)
{
}
