﻿namespace Airlines.Business;

public class InvalidReservationException(string message) : Exception(message)
{
}
