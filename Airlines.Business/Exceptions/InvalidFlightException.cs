﻿namespace Airlines.Business;

public class InvalidFlightException(string customMessage) : Exception(customMessage)
{
}
