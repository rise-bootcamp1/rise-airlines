﻿namespace Airlines.Business;

public class InvalidAircraftException(string customMessage) : Exception(customMessage)
{
}
