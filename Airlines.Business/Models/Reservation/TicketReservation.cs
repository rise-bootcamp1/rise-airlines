﻿namespace Airlines.Business;

public class TicketReservation : Reservation
{
    public int Seats { get; set; }
    public int SmallBaggageCount { get; set; }
    public int LargeBaggageCount { get; set; }
}
