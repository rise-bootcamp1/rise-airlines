﻿namespace Airlines.Business;

public class CargoReservation : Reservation
{
    public int CargoWeight { get; set; }
    public decimal CargoVolume { get; set; }
}
