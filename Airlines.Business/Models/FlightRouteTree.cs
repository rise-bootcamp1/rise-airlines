namespace Airlines.Business;

public class FlightRouteTree
{
    public class TreeNode(Flight flight)
    {
        public Airport Airport { get; } = flight.ArrivalAirport!;
        public Flight Flight { get; } = flight;
        public Dictionary<string, TreeNode> Children { get; } = [];
    }

    public Airport? StartAirport { get; set; }
    public Dictionary<string, TreeNode> Children { get; }
    public FlightRouteTree()
    {
        StartAirport = null;
        Children = [];
    }
    public void Add(Flight flight)
    {
        if (flight?.ArrivalAirport?.Id == StartAirport?.Id)
            throw new InvalidFlightException("Flight arrival airport is the same as the start airport");

        if (!AddRecursive(flight!, Children, StartAirport!))
            throw new InvalidFlightException("Flight already exists or has no connection to the start airport");
    }
    private static bool AddRecursive(Flight flight, Dictionary<string, TreeNode> children, Airport startAirport)
    {
        if (flight.DepartureAirport?.Equals(startAirport) == true)
        {
            if (!children.ContainsKey(flight.ArrivalAirport!.Id))
            {
                children.Add(flight.ArrivalAirport.Id, new TreeNode(flight));
                return true;
            }

            return false;
        }
        else
        {
            foreach (var child in children.Values)
            {
                if (AddRecursive(flight, child.Children, child.Airport))
                    return true;
            }

            return false;
        }
    }
    public List<Flight> FindPath(Airport destination)
    {
        var path = new List<Flight>();
        if (StartAirport == null)
            throw new InvalidOperationException("Start airport is not set");

        foreach (var child in Children.Values)
        {
            if (FindPathRecursive(child, destination, path))
            {
                path.Insert(0, child.Flight);
                return path;
            }
        }

        throw new NotFoundException("No path found to the destination airport");
    }

    private static bool FindPathRecursive(TreeNode treeNode, Airport destination, List<Flight> path)
    {
        if (treeNode.Airport.Equals(destination))
            return true;

        foreach (var child in treeNode.Children.Values)
        {
            if (FindPathRecursive(child, destination, path))
            {
                path.Insert(0, child.Flight); // Insert the flight at the beginning of the path
                return true;
            }
        }
        return false;
    }
    public void LoadFromFile(string path, AirportManager airportManager, FlightManager flightManager)
    {
        var data = new List<string>();
        using (var reader = new StreamReader(path))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine()?.Trim();
                if (!string.IsNullOrEmpty(line))
                    data.Add(line);
            }
        }

        StartAirport =
            airportManager.Airports.TryGetValue(data[0], out var airport) ?
            airport :
            throw new NotFoundException("Start airport not found");

        data.RemoveAt(0);

        foreach (var line in data)
        {
            if (flightManager.Flights.TryGetValue(line, out var flight))
                Add(flight);
            else
                throw new NotFoundException("Flight not found");
        }
    }
}