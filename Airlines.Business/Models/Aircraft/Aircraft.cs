﻿namespace Airlines.Business;

public abstract class Aircraft(string model)
{
    public string Model { get; protected set; } = model;
    public abstract string GetInfo();
}
