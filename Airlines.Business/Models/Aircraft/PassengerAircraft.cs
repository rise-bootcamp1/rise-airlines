﻿namespace Airlines.Business;

public class PassengerAircraft(string model) : Aircraft(model)
{
    public int CargoWeight { get; set; }
    public decimal CargoVolume { get; set; }
    public int Seats { get; set; }

    public override string GetInfo() => $"Model: {Model}, Cargo Weight: {CargoWeight}, Cargo Volume: {CargoVolume}, Seats: {Seats}";
}
