﻿namespace Airlines.Business;

public class CargoAircraft(string model) : Aircraft(model)
{
    public int CargoWeight { get; set; }
    public decimal CargoVolume { get; set; }

    public override string GetInfo() => $"Model: {Model}, Cargo Weight: {CargoWeight}, Cargo Volume: {CargoVolume}";
}
