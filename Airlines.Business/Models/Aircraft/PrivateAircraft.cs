﻿namespace Airlines.Business;

public class PrivateAircraft(string model) : Aircraft(model)
{
    public int Seats { get; set; }
    public override string GetInfo() => $"Model: {Model}, Seats: {Seats}";
}
