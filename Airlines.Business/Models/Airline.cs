﻿namespace Airlines.Business;

public class Airline
{
    private string name;
    public Airline() => name = string.Empty;
    public string Name
    {
        get => name;
        set
        {
            if (string.IsNullOrEmpty(value) || !IsNameValid(value))
                throw new InvalidAirlineException("Invalid airline name");
            name = value;
        }
    }
    public override bool Equals(object? obj)
    {
        if (obj is not Airline airline)
            return false;

        return Name == airline.Name;
    }
    public override int GetHashCode()
        => Name.GetHashCode();
    private static bool IsNameValid(string name)
        => name.Length is > 0 and < 6;
}
