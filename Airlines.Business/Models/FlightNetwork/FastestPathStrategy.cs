
namespace Airlines.Business;
public class FastestPathStrategy : IPathFindingStrategy
{
    public List<Flight> FindPath(FlightNetwork network, Airport source, Airport destination)
    {
        var duration = new Dictionary<Airport, TimeSpan>();
        var parentFlight = new Dictionary<Airport, Flight?>();
        var visited = new HashSet<Airport>();

        foreach (var airport in network.AdjacencyList.Keys)
        {
            duration[airport] = TimeSpan.MaxValue;
            parentFlight[airport] = null;
        }

        duration[source] = TimeSpan.Zero;

        while (visited.Count < network.AdjacencyList.Count)
        {
            Airport? current = null;
            var minDuration = TimeSpan.MaxValue;
            foreach (var airport in network.AdjacencyList.Keys)
            {
                if (!visited.Contains(airport) && duration[airport] < minDuration)
                {
                    current = airport;
                    minDuration = duration[airport];
                }
            }

            if (current == null)
                break;

            _ = visited.Add(current);

            foreach (var flight in network.AdjacencyList[current])
            {
                var newDuration = duration[current] + flight.Duration;
                if (newDuration < duration[flight.ArrivalAirport!])
                {
                    duration[flight.ArrivalAirport!] = newDuration;
                    parentFlight[flight.ArrivalAirport!] = flight;
                }
            }
        }

        var fastestPath = new List<Flight>();
        var currentAirport = destination;
        while (!currentAirport!.Equals(source))
        {
            var parent = parentFlight[currentAirport!] ??
                throw new NotFoundException("No path exists between the source and destination airports.");
            fastestPath.Insert(0, parent);
            currentAirport = parent.DepartureAirport;
        }

        return fastestPath;
    }
}