﻿using System.Collections.ObjectModel;

namespace Airlines.Business;

public class FlightNetwork
{
    private readonly Dictionary<Airport, List<Flight>> adjacencyList;
    public FlightNetwork()
    {
        adjacencyList = [];
        Strategy = new FewestStopsPathStrategy();
    }
    public FlightNetwork(IEnumerable<Airport> airports, IEnumerable<Flight> flights) : this()
    {
        foreach (var airport in airports)
            AddAirport(airport);

        foreach (var flight in flights)
            AddFlight(flight);
    }
    public ReadOnlyDictionary<Airport, List<Flight>> AdjacencyList => new(adjacencyList);
    public IPathFindingStrategy Strategy { get; set; }
    public void AddAirport(Airport airport)
    {
        if (!adjacencyList.ContainsKey(airport))
            adjacencyList.Add(airport, []);
    }
    public void AddFlight(Flight flight)
    {
        if (!adjacencyList.ContainsKey(flight.DepartureAirport!) || !adjacencyList.ContainsKey(flight.ArrivalAirport!))
            throw new NotFoundException("Departure or arrival airport not found in the network.");

        adjacencyList[flight.DepartureAirport!].Add(flight);
    }
    public List<Flight> FindPath(Airport source, Airport destination) => Strategy.FindPath(this, source, destination);
    public bool AreAirportsConnected(Airport source, Airport destination)
    {
        var visited = new HashSet<Airport>();

        bool DFS(Airport current, Airport destination, HashSet<Airport> visited)
        {
            if (current.Equals(destination))
                return true;

            _ = visited.Add(current);

            foreach (var flight in adjacencyList[current])
            {
                if (!visited.Contains(flight.ArrivalAirport!))
                {
                    if (DFS(flight.ArrivalAirport!, destination, visited))
                        return true;
                }
            }

            return false;
        }

        return DFS(source, destination, visited);
    }
}
