
namespace Airlines.Business;
public class FewestStopsPathStrategy : IPathFindingStrategy
{
    public List<Flight> FindPath(FlightNetwork network, Airport source, Airport destination)
    {
        var queue = new Queue<Airport>();
        var stops = new Dictionary<Airport, int>();
        var parentFlight = new Dictionary<Airport, Flight?>();
        var visited = new HashSet<Airport>();

        foreach (var airport in network.AdjacencyList.Keys)
        {
            stops[airport] = int.MaxValue;
            parentFlight[airport] = null;
        }

        queue.Enqueue(source);
        stops[source] = 0;

        while (queue.Count > 0)
        {
            var current = queue.Dequeue();

            if (current == destination)
                break;

            _ = visited.Add(current);

            foreach (var flight in network.AdjacencyList[current])
            {
                if (!visited.Contains(flight.ArrivalAirport!))
                {
                    var newStops = stops[current] + 1;
                    if (newStops < stops[flight.ArrivalAirport!])
                    {
                        stops[flight.ArrivalAirport!] = newStops;
                        parentFlight[flight.ArrivalAirport!] = flight;
                        queue.Enqueue(flight.ArrivalAirport!);
                    }
                }
            }
        }

        var fewestStopsPath = new List<Flight>();
        var currentAirport = destination;
        while (!currentAirport!.Equals(source))
        {
            var parent = parentFlight[currentAirport!] ??
                throw new NotFoundException("No path exists between the source and destination airports.");
            fewestStopsPath.Insert(0, parent);
            currentAirport = parent.DepartureAirport;
        }

        return fewestStopsPath;
    }
}