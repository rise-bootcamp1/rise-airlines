namespace Airlines.Business;
public interface IPathFindingStrategy
{
    List<Flight> FindPath(FlightNetwork network, Airport source, Airport destination);
}
