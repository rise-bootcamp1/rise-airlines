
namespace Airlines.Business;
public class CheapestPathStrategy : IPathFindingStrategy
{
    public List<Flight> FindPath(FlightNetwork network, Airport source, Airport destination)
    {
        var cost = new Dictionary<Airport, decimal>();
        var parentFlight = new Dictionary<Airport, Flight?>();
        var visited = new HashSet<Airport>();

        foreach (var airport in network.AdjacencyList.Keys)
        {
            cost[airport] = decimal.MaxValue;
            parentFlight[airport] = null;
        }

        cost[source] = 0;

        while (visited.Count < network.AdjacencyList.Count)
        {
            Airport? current = null;
            var minCost = decimal.MaxValue;
            foreach (var airport in network.AdjacencyList.Keys)
            {
                if (!visited.Contains(airport) && cost[airport] < minCost)
                {
                    current = airport;
                    minCost = cost[airport];
                }
            }

            if (current == null)
                break;

            _ = visited.Add(current);

            foreach (var flight in network.AdjacencyList[current])
            {
                var newCost = cost[current] + flight.Price;
                if (newCost < cost[flight.ArrivalAirport!])
                {
                    cost[flight.ArrivalAirport!] = newCost;
                    parentFlight[flight.ArrivalAirport!] = flight;
                }
            }
        }

        var cheapestPath = new List<Flight>();
        var currentAirport = destination;
        while (!currentAirport!.Equals(source))
        {
            var parent = parentFlight[currentAirport!] ??
                throw new NotFoundException("No path exists between the source and destination airports.");
            cheapestPath.Insert(0, parent);
            currentAirport = parent.DepartureAirport;
        }

        return cheapestPath;
    }
}