﻿using System.Text.RegularExpressions;

namespace Airlines.Business;

public partial class Airport
{
    private string id;
    private string name;
    private string city;
    private string country;
    public Airport() => id = name = city = country = string.Empty;
    public string Id
    {
        get => id;
        set
        {
            if (string.IsNullOrEmpty(value) || !IsValidId(value))
                throw new InvalidAirportException("Invalid airport ID");
            id = value;
        }
    }
    public string Name
    {
        get => name;
        set
        {
            if (string.IsNullOrEmpty(value) || !IsAlphabeticPlusSigns(value))
                throw new InvalidAirportException("Invalid airport name");
            name = value;
        }
    }
    public string City
    {
        get => city;
        set
        {
            if (string.IsNullOrEmpty(value) || !IsAlphabeticPlusSigns(value))
                throw new InvalidAirportException("Invalid airport city");
            city = value;
        }
    }
    public string Country
    {
        get => country;
        set
        {
            if (string.IsNullOrEmpty(value) || !IsAlphabeticPlusSigns(value))
                throw new InvalidAirportException("Invalid airport country");
            country = value;
        }
    }
    public override bool Equals(object? obj)
    {
        if (obj is not Airport airport)
            return false;

        return Id == airport.Id;
    }
    public override int GetHashCode()
        => Id.GetHashCode();
    private static bool IsValidId(string airportCode)
        => IdRegex().IsMatch(airportCode);
    private static bool IsAlphabeticPlusSigns(string name)
        => AlphabetPlusSignsRegex().IsMatch(name);

    [GeneratedRegex("^[A-Za-z0-9]{2,4}$")]
    private static partial Regex IdRegex();
    [GeneratedRegex("^[A-Za-z.' -]+$")]
    private static partial Regex AlphabetPlusSignsRegex();
}