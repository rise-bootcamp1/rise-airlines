﻿namespace Airlines.Business;

public class Route
{
    public Route() => Flights = new();
    public SinglyLinkedList Flights { get; private set; }
    public class SinglyLinkedList
    {
        public class Node
        {
            public Node() => Flight = null;

            public Node(Flight flight) => Flight = flight;
            public Flight? Flight { get; set; }
            public Node? Next { get; set; }
        }
        public SinglyLinkedList() => Head = Tail = null;
        public Node? Head { get; private set; }
        public Node? Tail { get; private set; }
        public void Add(Flight flight)
        {
            Node newNode = new(flight);
            if (Head == null)
            {
                Head = newNode;
                Tail = newNode;
            }
            else
            {
                Tail!.Next = newNode;
                Tail = newNode;
            }
        }
        public void Remove()
        {
            if (Head == null)
                return;
            if (Head == Tail)
            {
                Head = null;
                Tail = null;
            }
            else
            {
                var current = Head;
                while (current!.Next != Tail)
                    current = current.Next;
                current.Next = null;
                Tail = current;
            }
        }
    }
    public void Add(Flight flight) => Flights.Add(flight);
    public void Remove() => Flights.Remove();
    public List<Flight> GetFlights()
    {
        List<Flight> flights = [];
        var current = Flights.Head;
        while (current != null)
        {
            flights.Add(current.Flight!);
            current = current.Next;
        }
        return flights;
    }
}
