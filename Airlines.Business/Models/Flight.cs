﻿using System.Net;
using System.Text.RegularExpressions;

namespace Airlines.Business;

public partial class Flight
{
    private static readonly int smallBaggageMaxWeight = 15;
    private static readonly int largeBaggageMaxWeight = 30;
    private static readonly decimal smallBaggageMaxVolume = 0.045m;
    private static readonly decimal largeBaggageMaxVolume = 0.09m;
    private static readonly TimeSpan minFlightDuration = TimeSpan.FromMinutes(30);
    private static readonly TimeSpan maxFlightDuration = TimeSpan.FromHours(24);
    private string id;
    private Airport? departureAirport;
    private Airport? arrivalAirport;
    private Aircraft? aircraft;
    private decimal price;
    private TimeSpan duration;

    public Flight()
    {
        id = string.Empty;
        Reservations = [];
    }
    public string Id
    {
        get => id;
        set
        {
            if (string.IsNullOrEmpty(value) || !IsNameValid(value))
                throw new InvalidFlightException("Invalid flight ID");
            id = value;
        }
    }
    public Airport? DepartureAirport
    {
        get => departureAirport;
        set
        {
            if (value == null)
                throw new InvalidFlightException("Invalid departure airport");

            departureAirport = value;
        }
    }
    public Airport? ArrivalAirport
    {
        get => arrivalAirport;
        set
        {
            if (value == null)
                throw new InvalidFlightException("Invalid arrival airport");

            arrivalAirport = value;
        }
    }

    public Aircraft? Aircraft
    {
        get => aircraft;
        set
        {
            if (value == null)
                throw new InvalidFlightException("Invalid aircraft assigned to the flight");

            aircraft = value;
        }
    }
    public List<Reservation> Reservations { get; }
    public decimal Price
    {
        get => price;
        set => price =
            decimal.IsNegative(value) ?
            throw new InvalidFlightException("Price must be non negative") :
            value;
    }
    public TimeSpan Duration
    {
        get => duration;
        set
        {
            if (value < minFlightDuration || value > maxFlightDuration)
                throw new InvalidFlightException
                    ($"Duration must be between {minFlightDuration} minutes and {maxFlightDuration} hours");

            duration = value;
        }
    }

    public void AddReservation(Reservation reservation)
    {
        if ((reservation is CargoReservation && aircraft is not CargoAircraft) ||
            (reservation is TicketReservation && aircraft is not PassengerAircraft))
            throw new InvalidReservationException("Invalid reservation type");

        switch (aircraft)
        {
            case CargoAircraft:
                {
                    var totalWeight = Reservations.OfType<CargoReservation>().Sum(r => r.CargoWeight) + ((CargoReservation)reservation).CargoWeight;
                    var totalVolume = Reservations.OfType<CargoReservation>().Sum(r => r.CargoVolume) + ((CargoReservation)reservation).CargoVolume;

                    if (totalWeight > ((CargoAircraft)aircraft).CargoWeight || totalVolume > ((CargoAircraft)aircraft).CargoVolume)
                        throw new InvalidReservationException("Cargo weight or volume exceeded");
                    else
                        Reservations.Add(reservation);
                    break;
                }

            case PassengerAircraft:
                {
                    var seats = Reservations.OfType<TicketReservation>().Sum(r => r.Seats) + ((TicketReservation)reservation).Seats;
                    var totalBaggageWeight = Reservations.OfType<TicketReservation>().Sum(r => (r.SmallBaggageCount * smallBaggageMaxWeight) + (r.LargeBaggageCount * largeBaggageMaxWeight)) +
                        (((TicketReservation)reservation).SmallBaggageCount * smallBaggageMaxWeight) + (((TicketReservation)reservation).LargeBaggageCount * largeBaggageMaxWeight);
                    var totalBaggageVolume = Reservations.OfType<TicketReservation>().Sum(r => (r.SmallBaggageCount * smallBaggageMaxVolume) + (r.LargeBaggageCount * largeBaggageMaxVolume)) +
                        (((TicketReservation)reservation).SmallBaggageCount * smallBaggageMaxVolume) + (((TicketReservation)reservation).LargeBaggageCount * largeBaggageMaxVolume);

                    if (seats > ((PassengerAircraft)aircraft).Seats || totalBaggageWeight > ((PassengerAircraft)aircraft).CargoWeight || totalBaggageVolume > ((PassengerAircraft)aircraft).CargoVolume)
                        throw new InvalidReservationException("Seats, baggage weight or volume exceeded");
                    else
                        Reservations.Add(reservation);
                    break;
                }

            default:
                throw new InvalidReservationException("Invalid reservation type");
        }
    }
    public override bool Equals(object? obj)
    {
        if (obj is not Flight flight)
            return false;

        return Id == flight.Id;
    }
    public override int GetHashCode()
        => Id.GetHashCode();
    private static bool IsNameValid(string name)
        => FlightRegex().IsMatch(name);

    [GeneratedRegex("^[A-Za-z0-9]+$")]
    private static partial Regex FlightRegex();
}
