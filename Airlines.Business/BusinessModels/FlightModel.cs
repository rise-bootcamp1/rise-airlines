﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business;

public class FlightModel
{
    [StringLength(5, ErrorMessage = "{0} must be less than {1} characters")]
    [RegularExpression("^[A-Za-z0-9]+$", ErrorMessage = "The {0} field must be uppercase alphabetic characters only.")]
    [Required(ErrorMessage = "{0} is required")]
    public required string Code { get; set; }
    public required AirportModel DepartureAirport { get; set; }
    public required AirportModel ArrivalAirport { get; set; }
    [Required(ErrorMessage = "{0} is required.")]
    public required DateTime DepartureTime { get; set; }
    [Required(ErrorMessage = "{0} is required.")]
    public required DateTime ArrivalTime { get; set; }
}
