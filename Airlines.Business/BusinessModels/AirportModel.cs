﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business;

public class AirportModel
{
    [StringLength(3, ErrorMessage = "{0} must be less than {1} characters")]
    [RegularExpression("^[A-Za-z0-9]+$", ErrorMessage = "The {0} field must be uppercase alphabetic characters only.")]
    [Required(ErrorMessage = "{0} is required")]
    public required string Code { get; set; }
    [StringLength(50, ErrorMessage = "{0} must be less than {1} characters")]
    [Required(ErrorMessage = "{0} is required")]
    public required string Name { get; set; }
    [StringLength(20, ErrorMessage = "{0} must be less than {1} characters")]
    [Required(ErrorMessage = "{0} is required")]
    public required string Country { get; set; }
    [StringLength(50, ErrorMessage = "{0} must be less than {1} characters")]
    [Required(ErrorMessage = "{0} is required")]
    public required string City { get; set; }
    [Range(0, int.MaxValue, ErrorMessage = "{0} must be positive")]
    [Required(ErrorMessage = "{0} is required")]
    public required int RunwaysCount { get; set; }
    [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
    [Required(ErrorMessage = "{0} is required")]
    [DataType(DataType.Date)]
    public required DateOnly Founded { get; set; }
}
