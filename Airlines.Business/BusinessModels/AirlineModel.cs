﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business;

public class AirlineModel
{
    [StringLength(10, ErrorMessage = "{0} must be less than {1} characters")]
    [Required(ErrorMessage = "{0} is required")]
    public required string Name { get; set; }
    [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
    [Required(ErrorMessage = "{0} is required")]
    [DataType(DataType.Date)]
    public required DateOnly Founded { get; set; }
    [Range(0, int.MaxValue, ErrorMessage = "{0} must be positive")]
    [Required(ErrorMessage = "{0} is required")]
    public required int FleetSize { get; set; }
    [StringLength(500, ErrorMessage = "{0} must be less than {1} characters")]
    public required string Description { get; set; }
}
