﻿using Airlines.Business;
using System.Globalization;

namespace Airlines.Console;

public static class CommandFactory
{
    public static ICommand CreateCommandFromString(string? input)
    {
        if (input is null)
            throw new InvalidInputException("Invalid command");

        ICommand? command;
        if (input?.StartsWith("input") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 2)
                throw new InvalidInputException("No argument provided. use 'input <collection>'");

            command = InputCommand.CreateCommand(parts[1]);
        }
        else if (input?.StartsWith("search") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 2)
                throw new InvalidInputException("No argument provided. use 'search <ItemId>'");

            command = SearchCommand.CreateCommand(parts[1]);
        }
        else if (input?.StartsWith("sort") == true)
        {

            var parts = input.Split(' ');
            if (parts.Length is < 2 or > 3)
                throw new InvalidInputException("Invalid number of arguments provided. Usage: 'sort <collection> [ascending|descending]'");

            var type = SortType.Ascending;

            if (parts.Length == 3)
            {
                if (parts[2] == "descending")
                    type = SortType.Descending;
                else if (parts[2] != "ascending")
                    throw new InvalidInputException("Invalid sorting argument. Use 'ascending' or 'descending'.");
            }

            command = SortCommand.CreateCommand(parts[1], type);
        }
        else if (input == "print")
            command = PrintCommand.CreateCommand();
        else if (input?.StartsWith("exist") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 2)
                throw new InvalidInputException("No argument provided. use 'exist <airlineCode>'");

            command = ExistCommand.CreateCommand(parts[1]);
        }
        else if (input?.StartsWith("list") == true)
        {
            var args = input[5..];
            var lastSpaceIndex = args.LastIndexOf(' ');
            var name = args[..lastSpaceIndex];
            var type = args[(lastSpaceIndex + 1)..];

            command = ListCommand.CreateCommand(name, type);
        }
        else if (input == "route new")
            command = RouteCommand.NewCommand.CreateCommand();
        else if (input?.StartsWith("route add") == true)
            command = RouteCommand.AddCommand.CreateCommand(input.Split(' ')[2]);
        else if (input == "route remove")
            command = RouteCommand.RemoveCommand.CreateCommand();
        else if (input == "route print")
            command = RouteCommand.PrintCommand.CreateCommand();
        else if (input == "route clear")
            command = RouteCommand.ClearCommand.CreateCommand();
        else if (input?.StartsWith("route find") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 3)
                throw new InvalidInputException("No argument provided. use 'route find <airportId>'");

            command = RouteCommand.FindCommand.CreateCommand(parts[2]);
        }
        else if (input?.StartsWith("route check") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 4)
                throw new InvalidInputException("No argument provided. use 'route check <StartAirport> <EndAirport>'");

            if (!Program.AirportManager.Airports.TryGetValue(parts[2], out var start) || !Program.AirportManager.Airports.TryGetValue(parts[3], out var end))
                throw new NotFoundException("Start or end airport not found.");

            command = new RouteCommand.CheckCommand(start, end, Program.FlightNetwork);
        }
        else if (input?.StartsWith("route search") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 5)
                throw new InvalidInputException("No argument provided. use 'route search <StartAirport> <EndAirport>'");

            IPathFindingStrategy strategy = parts[4] switch
            {
                "cheap" => new FewestStopsPathStrategy(),
                "short" => new FastestPathStrategy(),
                "stops" => new FewestStopsPathStrategy(),
                _ => throw new InvalidInputException("Invalid strategy. Use cheap', 'short' or 'stops'."),
            };
            command = new RouteCommand.SearchCommand
            (
                Program.AirportManager.Airports[parts[2]],
                Program.AirportManager.Airports[parts[3]],
                Program.FlightNetwork,
                strategy
            );
        }
        else if (input?.StartsWith("reserve cargo") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 5)
                throw new InvalidInputException("Invalid command");

            command = ReserveCommand.CargoCommand.CreateCommand
            (
                parts[2],
                int.Parse(parts[3]),
                decimal.Parse(parts[4], CultureInfo.InvariantCulture)
            );
        }
        else if (input?.StartsWith("reserve ticket") == true)
        {
            var parts = input.Split(' ');
            if (parts.Length != 6)
                throw new InvalidInputException("Invalid command");

            command = ReserveCommand.TicketCommand.CreateCommand
            (
                parts[2],
                int.Parse(parts[3]),
                int.Parse(parts[4]),
                int.Parse(parts[5])
            );
        }
        else if (input == "batch start")
        {
            command = BatchCommand.StartCommand.CreateCommand(Program.CommandSequence);
            Program.IsBatchMode = true;
        }
        else if (input == "batch run")
            command = BatchCommand.RunCommand.CreateCommand(Program.CommandSequence);
        else if (input == "batch cancel")
        {
            command = BatchCommand.CancelCommand.CreateCommand(Program.CommandSequence);
            Program.IsBatchMode = false;
        }
        else
            throw new InvalidInputException("Invalid command");

        return command;
    }
}