﻿namespace Airlines.Business;

public interface IConsoleService
{
    void WriteLine(string message);
    void Write(string message);
    string? ReadLine();
}
