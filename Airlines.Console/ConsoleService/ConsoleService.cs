﻿namespace Airlines.Business;

public class ConsoleService : IConsoleService
{
    public void WriteLine(string message) => System.Console.WriteLine(message);
    public void Write(string message) => System.Console.Write(message);
    public string? ReadLine() => System.Console.ReadLine();
}