﻿using Airlines.Business;
using Airlines.Persistance;
using Microsoft.Extensions.Configuration;

namespace Airlines.Console;

public class Program
{
    public static ConsoleService ConsoleService { get; } = new();
    public static AirportManager AirportManager { get; } = new();
    public static AirlineManager AirlineManager { get; } = new();
    public static FlightManager FlightManager { get; } = new();
    public static AircraftManager AircraftManager { get; } = new();
    public static Route? Route { get; set; }
    public static bool IsBatchMode { get; set; }
    public static Queue<ICommand> CommandSequence { get; } = new();
    public static FlightRouteTree FlightRouteTree { get; } = new();
    public static FlightNetwork FlightNetwork { get; set; } = new();
    public enum InputType
    {
        Airport,
        Airline,
        Flight
    }
    public static void CommandLine()
    {
        while (true)
        {
            ConsoleService.Write(":> ");
            var input = ConsoleService.ReadLine()?.Trim();

            if (input is null)
                continue;

            if (input == "exit")
                break;

            ICommand command;
            try
            {
                command = CommandFactory.CreateCommandFromString(input);
            }
            catch (Exception e)
            {
                ConsoleService.WriteLine(e.Message);
                continue;
            }

            if (IsBatchMode)
            {
                if (command is BatchCommand.RunCommand)
                {
                    command = new BatchCommand.RunCommand(CommandSequence);
                }
                else if (command is BatchCommand.CancelCommand)
                {
                    command = new BatchCommand.CancelCommand(CommandSequence);
                    IsBatchMode = false;
                }
                else
                {
                    CommandSequence.Enqueue(command!);
                    continue;
                }
            }

            try
            {
                command?.Execute();
            }
            catch (Exception e)
            {
                ConsoleService.WriteLine(e.Message);
            }
        }
        ConsoleService.WriteLine("Goodbye!");
    }
    private static void LoadData()
    {
        AirportManager.ParseAirports(FileReader.ReadFile(".\\SeedData\\AirportsData.csv"));
        AirlineManager.ParseAirlines(FileReader.ReadFile(".\\SeedData\\AirlinesData.csv"));
        AircraftManager.ParseAircrafts(FileReader.ReadFile(".\\SeedData\\AircraftsData.csv"));
        FlightManager.ParseFlights(FileReader.ReadFile(".\\SeedData\\FlightsData.csv"), AirportManager, AircraftManager);
    }
    // private static IConfiguration CreateConfig()
    // {
    //     IConfiguration configuration = new ConfigurationBuilder()
    //         .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    //         .Build();

    //     return configuration;
    // }
    private static void ConsoleRun()
    {
        LoadData();
        FlightRouteTree.LoadFromFile(".\\SeedData\\FlightRouteTree.csv", AirportManager, FlightManager);
        FlightNetwork = new FlightNetwork(AirportManager.Airports.Values, FlightManager.Flights.Values);
        CommandLine();
    }
    // private static void EFTest()
    // {
    //     var config = CreateConfig();
    //     using var context = new RiseAirlinesContext(config);
    //     using var airportRepository = new AirportRepository(context);
    //     using var airlineRepository = new AirlineRepository(context);
    //     using var flightRepository = new FlightRepository(context);
    //     airportRepository.GetAll().ForEach(a => ConsoleService.WriteLine(a.Code.ToString()));
    //     airlineRepository.GetAll().ForEach(a => ConsoleService.WriteLine(a.Id.ToString()));
    //     flightRepository.GetAll().ForEach(a => ConsoleService.WriteLine(a.Code.ToString()));
    //     flightRepository.Delete(256);
    //     flightRepository.Add(new FlightEntity { Code = "TEST", Departure = 1, Arrival = 2, DepartureDateTime = DateTime.Now.AddHours(1), ArrivalDateTime = DateTime.Now.AddHours(2) });
    // }
    public static void Main() => ConsoleRun();//EFTest();
}