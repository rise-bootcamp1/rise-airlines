﻿using Airlines.Business;

namespace Airlines.Console;

public class ListCommand : ICommand
{
    private readonly string name;
    private readonly string type;
    public ListCommand(string name, string type)
    {
        if (type is not "City" and not "Country")
            throw new NotFoundException("List command type not found. Use 'City' or 'Country'");

        this.name = name;
        this.type = type;
    }
    private List<Airport> GetAirportsByType(string type)
    {
        if (type == "City")
            return Program.AirportManager.GetByCity(name);
        else
            return Program.AirportManager.GetByCountry(name);
    }
    public void Execute()
    {
        var airports = GetAirportsByType(type);

        Program.ConsoleService.WriteLine($"\nAirports in {name}");

        foreach (var airport in airports)
            Program.ConsoleService.WriteLine(
                $"Identifier: {airport.Id}, Name: {airport.Name}, City: {airport.City}, Country: {airport.Country}");
    }
    public static ICommand CreateCommand(string name, string type)
        => new ListCommand(name, type);
}
