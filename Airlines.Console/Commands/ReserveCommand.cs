﻿using Airlines.Business;

namespace Airlines.Console;

public class ReserveCommand
{
    public class CargoCommand(string flightId, int cargoWeight, decimal cargoVolume) : ICommand
    {
        private readonly string flightId = flightId;
        private readonly int cargoWeight = cargoWeight;
        private readonly decimal cargoVolume = cargoVolume;

        public void Execute()
        {
            if (!Program.FlightManager.Flights.ContainsKey(flightId))
                throw new NotFoundException("Flight not found");

            Program.FlightManager.Flights[flightId].AddReservation(new CargoReservation
            {
                CargoWeight = cargoWeight,
                CargoVolume = cargoVolume
            });

            Program.ConsoleService.WriteLine("Successful reservation");
        }
        public static ICommand CreateCommand(string flightId, int cargoWeight, decimal cargoVolume)
            => new CargoCommand(flightId, cargoWeight, cargoVolume);
    }
    public class TicketCommand(string flightId, int seats, int smallBaggageCount, int largeBaggageCount) : ICommand
    {
        private readonly string flightId = flightId;
        private readonly int seats = seats;
        private readonly int smallBaggageCount = smallBaggageCount;
        private readonly int largeBaggageCount = largeBaggageCount;

        public void Execute()
        {
            if (!Program.FlightManager.Flights.ContainsKey(flightId))
                throw new NotFoundException("Flight not found");

            Program.FlightManager.Flights[flightId].AddReservation(new TicketReservation
            {
                Seats = seats,
                SmallBaggageCount = smallBaggageCount,
                LargeBaggageCount = largeBaggageCount
            });

            Program.ConsoleService.WriteLine("Successful reservation");
        }
        public static ICommand CreateCommand(string flightId, int seats, int smallBaggageCount, int largeBaggageCount)
            => new TicketCommand(flightId, seats, smallBaggageCount, largeBaggageCount);
    }
}
