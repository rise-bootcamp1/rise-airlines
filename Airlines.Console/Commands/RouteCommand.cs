using Airlines.Business;

namespace Airlines.Console;

public class RouteCommand
{
    public class NewCommand : ICommand
    {
        public void Execute()
            => Program.Route = new Route();
        public static ICommand CreateCommand()
            => new NewCommand();
    }
    public class AddCommand(string flightId) : ICommand
    {
        private readonly string flightId = flightId;

        public void Execute()
        {
            if (Program.Route == null)
            {
                Program.ConsoleService.WriteLine("Route not created. First run 'route new' command");
                return;
            }

            Flight flight;
            try
            {
                flight = Program.FlightManager.Flights[flightId];
            }
            catch (KeyNotFoundException)
            {
                Program.ConsoleService.WriteLine("Flight not found");
                return;
            }

            var lastFlight = Program.Route.Flights.Tail?.Flight;

            if (lastFlight != null && lastFlight.ArrivalAirport != flight.DepartureAirport)
            {
                Program.ConsoleService.WriteLine("Invalid flight");
                return;
            }

            Program.Route.Add(flight);
        }
        public static ICommand CreateCommand(string flightId)
            => new AddCommand(flightId);
    }
    public class PrintCommand : ICommand
    {
        public void Execute()
        {
            if (Program.Route == null)
            {
                Program.ConsoleService.WriteLine("Route not created");
                return;
            }

            foreach (var flight in Program.Route.GetFlights())
                Program.ConsoleService.WriteLine($"Id: {flight.Id}");
        }
        public static ICommand CreateCommand()
            => new PrintCommand();
    }
    public class ClearCommand : ICommand
    {
        public void Execute()
            => Program.Route = null;
        public static ICommand CreateCommand()
            => new ClearCommand();
    }
    public class RemoveCommand : ICommand
    {
        public void Execute()
        {
            if (Program.Route == null)
            {
                Program.ConsoleService.WriteLine("Route not created");
                return;
            }

            Program.Route.Remove();
        }
        public static ICommand CreateCommand()
            => new RemoveCommand();
    }
    public class FindCommand(string destinationId) : ICommand
    {
        private readonly string destinationId = destinationId;

        public void Execute()
        {
            if (Program.AirportManager.Airports.TryGetValue(destinationId, out var destination))
            {
                var path = Program.FlightRouteTree.FindPath(destination);
                Program.ConsoleService.WriteLine("Flight path to " + destination.Name + ":");

                foreach (var flight in path)
                {
                    Program.ConsoleService.WriteLine($"Flight {flight.Id} from {flight?.DepartureAirport?.Id} to {flight?.ArrivalAirport?.Id}");
                }
            }
            else
            {
                Program.ConsoleService.WriteLine("Destination airport not found");
                return;
            }
        }
        public static ICommand CreateCommand(string destinationId)
            => new FindCommand(destinationId);
    }
    public class CheckCommand(Airport start, Airport end, FlightNetwork network) : ICommand
    {
        private readonly Airport start = start;
        private readonly Airport end = end;
        private readonly FlightNetwork network = network;

        public void Execute() => Program.ConsoleService.WriteLine(network.AreAirportsConnected(start, end).ToString());
    }
    public class SearchCommand(Airport start, Airport end, FlightNetwork network, IPathFindingStrategy strategy) : ICommand
    {
        private readonly Airport start = start;
        private readonly Airport end = end;
        private readonly FlightNetwork network = network;
        private readonly IPathFindingStrategy strategy = strategy;
        public void Execute()
        {
            network.Strategy = strategy;
            var path = network.FindPath(start, end);
            path.ForEach(airport => Program.ConsoleService.WriteLine(airport.Id));
        }
    }
}
