﻿using Airlines.Business;

namespace Airlines.Console;

public class InputCommand : ICommand
{
    private readonly string type;
    public InputCommand(string type)
    {
        if (type is not "all" and not "airports" and not "airlines" and not "flights")
            throw new InvalidInputException("Invalid collection name");

        this.type = type;
    }
    private static Airport ParseToAirport(string input)
    {
        var parts = input.Trim().Split(',');
        if (parts.Length != 4)
            throw new InvalidInputException("Invalid airport format");

        return new Airport
        {
            Id = parts[0].Trim(),
            Name = parts[1].Trim(),
            City = parts[2].Trim(),
            Country = parts[3].Trim()
        };
    }
    private static Flight ParseToFlight(string input)
    {
        var parts = input.Trim().Split(',');
        if (parts.Length != 4)
            throw new InvalidInputException("Invalid flight format");

        if (Program.FlightManager.Flights.ContainsKey(parts[0].Trim()))
            throw new DuplicateException("Flight with the same ID already exists");

        if (!Program.AirportManager.Airports.ContainsKey(parts[1].Trim()) || !Program.AirportManager.Airports.ContainsKey(parts[2].Trim()))
            throw new NotFoundException("Airport not found");

        if (!Program.AircraftManager.Aircrafts.ContainsKey(parts[3].Trim()))
            throw new NotFoundException("Aircraft not found");

        return new Flight
        {
            Id = parts[0].Trim(),
            DepartureAirport = Program.AirportManager.Airports[parts[1].Trim()],
            ArrivalAirport = Program.AirportManager.Airports[parts[2].Trim()],
            Aircraft = Program.AircraftManager.Aircrafts[parts[3].Trim()]
        };
    }
    private static void ReadInput(Program.InputType inputType)
    {
        Program.ConsoleService.WriteLine($"Enter {inputType}. Type 'done' to finish.");
        switch (inputType)
        {
            case Program.InputType.Airport:
                Program.ConsoleService.WriteLine("Format: <Identifier>,<Name>,<City>,<Country>");
                break;
            case Program.InputType.Airline:
                Program.ConsoleService.WriteLine("Format: <Name>");
                break;
            case Program.InputType.Flight:
                Program.ConsoleService.WriteLine("Format: <Code>,<DepartureAirport>,<ArrivalAirport>,<Aircraft>");
                break;
            default:
                break;
        }

        string? input;
        Program.ConsoleService.Write("New entry: ");
        while ((input = Program.ConsoleService.ReadLine()) != null && input != "done")
        {
            try
            {
                switch (inputType)
                {
                    case Program.InputType.Airport:
                        Program.AirportManager.Add(ParseToAirport(input));
                        break;
                    case Program.InputType.Airline:
                        Program.AirlineManager.Add(new Airline { Name = input });
                        break;
                    case Program.InputType.Flight:
                        Program.FlightManager.Add(ParseToFlight(input));
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Program.ConsoleService.WriteLine(e.Message);
            }

            Program.ConsoleService.Write("New entry: ");
        }
    }
    public void Execute()
    {
        if (type == "all")
        {
            ReadInput(Program.InputType.Airport);
            ReadInput(Program.InputType.Airline);
            ReadInput(Program.InputType.Flight);
        }
        else if (type == "airports")
            ReadInput(Program.InputType.Airport);
        else if (type == "airlines")
            ReadInput(Program.InputType.Airline);
        else if (type == "flights")
            ReadInput(Program.InputType.Flight);
        else
            throw new InvalidInputException("Invalid input type");
    }
    public static ICommand CreateCommand(string type)
        => new InputCommand(type);
}
