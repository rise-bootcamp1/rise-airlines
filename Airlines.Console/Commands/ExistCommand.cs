﻿namespace Airlines.Console;

public class ExistCommand(string airlineName) : ICommand
{
    private readonly string airlineName = airlineName;

    public void Execute()
        => Program.ConsoleService.WriteLine(Program.AirlineManager.Airlines.ContainsKey(airlineName) ?
            "Airline exists" :
            "Airline does not exist");
    public static ICommand CreateCommand(string airlineName)
        => new ExistCommand(airlineName);
}
