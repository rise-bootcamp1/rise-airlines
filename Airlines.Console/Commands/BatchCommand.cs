namespace Airlines.Console;

public class BatchCommand
{
    public class StartCommand(Queue<ICommand> commandSequence) : ICommand
    {
        public void Execute()
            => commandSequence = new Queue<ICommand>();
        public static ICommand CreateCommand(Queue<ICommand> commandSequence)
            => new StartCommand(commandSequence);
    }
    public class RunCommand(Queue<ICommand> commandSequence) : ICommand
    {
        private readonly Queue<ICommand> commandSequence = commandSequence;
        public void Execute()
        {
            while (commandSequence.Count > 0)
            {
                var currentCommand = commandSequence.Dequeue();
                currentCommand.Execute();
            }
        }
        public static ICommand CreateCommand(Queue<ICommand> commandSequence)
            => new RunCommand(commandSequence);
    }
    public class CancelCommand(Queue<ICommand>? commandSequence) : ICommand
    {
        public void Execute()
            => commandSequence = null;
        public static ICommand CreateCommand(Queue<ICommand>? commandSequence)
            => new CancelCommand(commandSequence);
    }
}