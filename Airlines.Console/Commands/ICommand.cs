﻿namespace Airlines.Console;

public interface ICommand
{
    void Execute();
}
