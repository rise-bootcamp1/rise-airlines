﻿namespace Airlines.Console;

public class SearchCommand(string searchString) : ICommand
{
    private readonly string searchString = searchString;
    public void Execute()
    {
        if (Program.AirportManager.Airports.ContainsKey(searchString))
        {
            Program.ConsoleService.WriteLine("Airport found");
            return;
        }

        if (Program.AirlineManager.Airlines.ContainsKey(searchString))
        {
            Program.ConsoleService.WriteLine("Airline found");
            return;
        }

        if (Program.FlightManager.Flights.ContainsKey(searchString))
        {
            Program.ConsoleService.WriteLine("Flight found");
            return;
        }

        Program.ConsoleService.WriteLine("Item not found");
    }
    public static ICommand CreateCommand(string searchString)
        => new SearchCommand(searchString);
}
