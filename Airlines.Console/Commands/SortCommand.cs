﻿using Airlines.Business;

namespace Airlines.Console;

public class SortCommand : ICommand
{
    private readonly string collectionName;
    private readonly SortType type;
    public SortCommand(string collectionName, SortType type)
    {
        if (collectionName is not "airports" and not "airlines" and not "flights")
            throw new InvalidInputException("Invalid collection name");

        this.collectionName = collectionName;
        this.type = type;
    }
    public void Execute()
    {
        if (collectionName == "airports")
            Program.AirportManager.Sort(type);
        else if (collectionName == "airlines")
            Program.AirlineManager.Sort(type);
        else if (collectionName == "flights")
            Program.FlightManager.Sort(type);
    }
    public static ICommand CreateCommand(string collectionName, SortType type)
        => new SortCommand(collectionName, type);
}
