﻿namespace Airlines.Console;

public class PrintCommand : ICommand
{
    public void Execute()
    {
        Program.ConsoleService.WriteLine("\nAirports:");
        foreach (var airport in Program.AirportManager.Airports)
            Program.ConsoleService.WriteLine(
                $"Identifier: {airport.Value.Id}, Name: {airport.Value.Name}, City: {airport.Value.City}, Country: {airport.Value.Country}");

        Program.ConsoleService.WriteLine("\nAirlines:");
        foreach (var airline in Program.AirlineManager.Airlines)
            Program.ConsoleService.WriteLine($"Name: {airline.Value.Name}");

        Program.ConsoleService.WriteLine("\nFlights:");
        foreach (var flight in Program.FlightManager.Flights)
            Program.ConsoleService.WriteLine($"Id: {flight.Value.Id}, Departure: {flight.Value.DepartureAirport?.Id}, Arrival: {flight.Value.ArrivalAirport?.Id}, Aircraft: {flight.Value.Aircraft?.Model}");

        Program.ConsoleService.WriteLine("\nAircrafts:");
        foreach (var aircraft in Program.AircraftManager.Aircrafts)
        {
            Program.ConsoleService.WriteLine(aircraft.Value.GetInfo());
        }
    }
    public static ICommand CreateCommand()
        => new PrintCommand();
}
