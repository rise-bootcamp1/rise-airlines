using Airlines.Business;
using Airlines.Persistance;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAutoMapper(typeof(MappingPatterns));
builder.Services.AddDbContext<RiseAirlinesContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("HomeConnection")));

builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();
builder.Services.AddScoped<IAirportRepository, AirportRepository>();
builder.Services.AddScoped<IFlightRepository, FlightRepository>();

builder.Services.AddScoped<IAirlinesService, AirlinesService>();
builder.Services.AddScoped<IAirportsService, AirportsService>();
builder.Services.AddScoped<IFlightsService, FlightsService>();

builder.Services.AddCors(options =>
{
    options.AddPolicy
    (
        name: "_allowAnyOriginPolicy",
        p => p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()
    );
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
