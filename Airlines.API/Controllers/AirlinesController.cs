using Airlines.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirlinesController(IAirlinesService airlineService) : ControllerBase
{
    private readonly IAirlinesService airlineService = airlineService;

    [HttpGet]
    public async Task<ActionResult<List<AirlineModel>>> GetAll()
    {
        var result = await airlineService.GetAirlinesAsync();

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value.IsNullOrEmpty())
            return NoContent();

        return Ok(result.Value);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<AirlineModel>> GetOne(int id)
    {
        var result = await airlineService.GetAirlineByIdAsync(id);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return NotFound();

        return Ok(result.Value);
    }

    [HttpPost]
    public async Task<ActionResult<AirlineModel>> Create([FromBody] AirlineModel airline)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var result = await airlineService.AddAirlineAsync(airline);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return BadRequest();

        return CreatedAtAction("Create", result.Value);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<AirlineModel>> Delete(int id)
    {
        var result = await airlineService.DeleteAirlineAsync(id);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return NotFound();

        return Ok(result);
    }

    [HttpPut]
    public async Task<ActionResult<AirlineModel>> Update([FromBody] AirlineModel airline)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var result = await airlineService.UpdateAirlineAsync(airline);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return BadRequest();

        return Ok(result);
    }
}