using Airlines.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirportsController(IAirportsService airportsService) : ControllerBase
{
    private readonly IAirportsService airportsService = airportsService;

    [HttpGet]
    public async Task<ActionResult<List<AirportModel>>> GetAll()
    {
        var result = await airportsService.GetAirportsAsync();

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value.IsNullOrEmpty())
            return NoContent();

        return Ok(result.Value);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<AirportModel>> GetOne(int id)
    {
        var result = await airportsService.GetAirportByIdAsync(id);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return NotFound();

        return Ok(result.Value);
    }

    [HttpPost]
    public async Task<ActionResult<AirportModel>> Create([FromBody] AirportModel airport)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var result = await airportsService.AddAirportAsync(airport);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return BadRequest();

        return CreatedAtAction("Create", result.Value);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<AirportModel>> Delete(int id)
    {
        var result = await airportsService.DeleteAirportAsync(id);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return NotFound();

        return Ok(result);
    }

    [HttpPut]
    public async Task<ActionResult<AirportModel>> Update([FromBody] AirportModel airport)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var result = await airportsService.UpdateAirportAsync(airport);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return BadRequest();

        return Ok(result);
    }
}