using Airlines.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FlightsController(IFlightsService flightsService) : ControllerBase
{
    private readonly IFlightsService flightsService = flightsService;

    [HttpGet]
    public async Task<ActionResult<List<FlightModel>>> GetAll()
    {
        var result = await flightsService.GetFlightsAsync();

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value.IsNullOrEmpty())
            return NoContent();

        return Ok(result.Value);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<FlightModel>> GetOne(int id)
    {
        var result = await flightsService.GetFlightByIdAsync(id);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return NotFound();

        return Ok(result.Value);
    }

    [HttpPost]
    public async Task<ActionResult<FlightModel>> Create([FromBody] FlightModel flight)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var result = await flightsService.AddFlightAsync(flight);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return BadRequest();

        return CreatedAtAction("Create", result.Value);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<AirportModel>> Delete(int id)
    {
        var result = await flightsService.DeleteFlightAsync(id);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return NotFound();

        return Ok(result);
    }

    [HttpPut]
    public async Task<ActionResult<AirportModel>> Update([FromBody] FlightModel flight)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var result = await flightsService.UpdateFlightAsync(flight);

        if (!result.IsSuccess)
            return StatusCode(500, new { message = "Internal server error" });

        if (result.Value == null)
            return BadRequest();

        return Ok(result);
    }
}