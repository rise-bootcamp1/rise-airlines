namespace Airlines.Web.Models;
public class SearchViewModel
{
    public string? ControllerName { get; set; }
    public string? ActionName { get; set; }
}