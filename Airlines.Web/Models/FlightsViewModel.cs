using Airlines.Business;

namespace Airlines.Web.Models;
public class FlightsViewModel
{
    public IEnumerable<FlightModel>? Flights { get; set; }
    public string? SearchValue { get; set; }
    public FlightModel? NewFlight { get; set; }
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }
}