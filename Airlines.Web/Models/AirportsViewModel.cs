using Airlines.Business;

namespace Airlines.Web.Models;
public class AirportsViewModel
{
    public IEnumerable<AirportModel>? Airports { get; set; }
    public string? SearchValue { get; set; }
    public AirportModel? NewAirport { get; set; }
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }
}