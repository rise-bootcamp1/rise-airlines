namespace Airlines.Web.Models;

public class DashboardStatsModel
{
    public int AirlineCount { get; set; }
    public int FlightCount { get; set; }
    public int AirportCount { get; set; }
}