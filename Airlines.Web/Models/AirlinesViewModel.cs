using Airlines.Business;

namespace Airlines.Web.Models;
public class AirlinesViewModel
{
    public IEnumerable<AirlineModel>? Airlines { get; set; }
    public string? SearchValue { get; set; }
    public AirlineModel? NewAirline { get; set; }
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }
}