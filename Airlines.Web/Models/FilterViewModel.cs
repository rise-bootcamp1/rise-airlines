namespace Airlines.Web.Models;
public class FilterViewModel
{
    public string? ControllerName { get; set; }
    public string? ActionName { get; set; }
    public IEnumerable<string>? FilterOptions { get; set; }
}