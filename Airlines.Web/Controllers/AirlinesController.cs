using Airlines.Business;
using Airlines.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class AirlinesController(IAirlinesService service) : Controller
    {
        private readonly IAirlinesService service = service;
        private async Task<AirlinesViewModel> GetPagedAirlinesAsync(int page = 1, int pageSize = 10, string? filter = null, string? value = null)
        {
            var totalCount = await service.GetAirlinesCountAsync();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            IEnumerable<AirlineModel> airlines;

            if (filter != null && value != null)
            {
                airlines = filter switch
                {
                    "FleetSize" => await service.GetAirlinesFilteredByFleetPageAsync(page, pageSize, int.Parse(value)),
                    "Founded" => await service.GetAirlinesFilteredByFoundedPageAsync(page, pageSize, DateOnly.Parse(value)),
                    _ => throw new ArgumentException("Invalid filter"),
                };
            }
            else if (value != null)
                airlines = await service.GetAirlinesByNamePageAsync(page, pageSize, value);
            else
            {
                airlines = await service.GetAirlinesPageAsync(page, pageSize);
            }

            var model = new AirlinesViewModel
            {
                Airlines = airlines,
                NewAirline = null,
                CurrentPage = page,
                TotalPages = totalPages,
            };

            return model;
        }
        public async Task<ActionResult> Index(int page = 1)
        {
            try
            {
                var viewModel = await GetPagedAirlinesAsync(page);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                return View();
            }
        }
        public async Task<IActionResult> Filter(string filter, string value)
        {
            var viewModel = await GetPagedAirlinesAsync(1, 5, filter, value);
            return View("Index", viewModel);
        }
        public async Task<IActionResult> Search(string searchTerm)
        {
            var viewModel = await GetPagedAirlinesAsync(1, 5, null, searchTerm);
            return View("Index", viewModel);
        }
        [HttpPost]
        public async Task<ActionResult> Create(AirlinesViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await service.AddAirlineAsync(viewModel.NewAirline!);
                    return RedirectToAction("Index");
                }

                TempData["ErrorMessage"] = "The Airline was not added";
                viewModel = await GetPagedAirlinesAsync();
                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                return RedirectToAction("Index");
            }
        }
    }
}
