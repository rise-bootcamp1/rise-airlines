using Airlines.Business;
using Airlines.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class AirportsController(IAirportsService service) : Controller
    {
        private readonly IAirportsService service = service;
        private async Task<AirportsViewModel> GetPagedAirportsAsync(int page = 1, int pageSize = 5, string? filter = null, string? value = null)
        {
            var totalCount = await service.GetAirportsCountAsync();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            IEnumerable<AirportModel> airports;

            if (filter != null && value != null)
            {
                airports = filter switch
                {
                    "City" => await service.GetAirportsFilteredByCityPageAsync(page, pageSize, value),
                    "Country" => await service.GetAirportsFilteredByCountryPageAsync(page, pageSize, value),
                    _ => throw new ArgumentException("Invalid filter"),
                };
            }
            else if (value != null)
                airports = await service.GetAirportsByNamePageAsync(page, pageSize, value);
            else
            {
                airports = await service.GetAirportsPageAsync(page, pageSize);
            }

            var model = new AirportsViewModel
            {
                Airports = airports,
                NewAirport = null,
                CurrentPage = page,
                TotalPages = totalPages,
            };

            return model;
        }
        [HttpPost]
        public async Task<ActionResult> Create(AirportsViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await service.AddAirportAsync(viewModel.NewAirport!);
                    return RedirectToAction("Index");
                }

                TempData["ErrorMessage"] = "The Airport was not added";
                viewModel = await GetPagedAirportsAsync();
                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                return RedirectToAction("Index");
            }
        }
        public async Task<IActionResult> Filter(string filter, string value)
        {
            var viewModel = await GetPagedAirportsAsync(1, 5, filter, value);
            return View("Index", viewModel);
        }
        public async Task<IActionResult> Search(string searchTerm)
        {
            var viewModel = await GetPagedAirportsAsync(1, 5, null, searchTerm);
            return View("Index", viewModel);
        }
        public async Task<ActionResult> Index(int page = 1)
        {
            try
            {
                var viewModel = await GetPagedAirportsAsync(page);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                return View();
            }
        }
    }
}
