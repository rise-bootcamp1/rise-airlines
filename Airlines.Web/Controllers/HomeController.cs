using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Airlines.Web.Models;
using Airlines.Business;

namespace Airlines.Web.Controllers;

public class HomeController(IAirlinesService airlinesService, IAirportsService airportsService, IFlightsService flightService) : Controller
{
    public readonly IAirlinesService airlinesService = airlinesService;
    public readonly IAirportsService airportsService = airportsService;
    public readonly IFlightsService flightService = flightService;
    public async Task<IActionResult> Index()
    {
        var model = new DashboardStatsModel
        {
            AirlineCount = await airlinesService.GetAirlinesCountAsync(),
            FlightCount = await flightService.GetFlightsCountAsync(),
            AirportCount = await airportsService.GetAirportsCountAsync()
        };
        return View(model);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
        => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
}
