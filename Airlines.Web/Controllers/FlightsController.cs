using Airlines.Business;
using Airlines.Persistance;
using Airlines.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Controllers
{
    public class FlightsController(IFlightsService service) : Controller
    {
        private readonly IFlightsService service = service;
        private async Task<FlightsViewModel> GetPagedFlightsAsync(int page = 1, int pageSize = 5, string? filter = null, string? value = null)
        {
            var totalCount = await service.GetFlightsCountAsync();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            IEnumerable<FlightModel> flights;

            if (filter != null && value != null)
            {
                flights = filter switch
                {
                    "Departure" => await service.GetFlightsFilteredByDeparturePageAsync(page, pageSize, value),
                    "Arrival" => await service.GetFlightsFilteredByArrivalPageAsync(page, pageSize, value),
                    _ => throw new ArgumentException("Invalid filter"),
                };
            }
            else if (value != null)
                flights = await service.GetFlightsByCodePageAsync(page, pageSize, value);
            else
            {
                flights = await service.GetFlightsPageAsync(page, pageSize);
            }

            var model = new FlightsViewModel
            {
                Flights = flights,
                NewFlight = null,
                CurrentPage = page,
                TotalPages = totalPages,
            };

            return model;
        }
        [HttpPost]
        public async Task<ActionResult> Create(FlightsViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await service.AddFlightAsync(viewModel.NewFlight!);
                    return RedirectToAction("Index");
                }

                TempData["ErrorMessage"] = "The Airport was not added";
                viewModel = await GetPagedFlightsAsync();
                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                return RedirectToAction("Index");
            }
        }
        public async Task<IActionResult> Filter(string filter, string value)
        {
            var viewModel = await GetPagedFlightsAsync(1, 5, filter, value);
            return View("Index", viewModel);
        }
        public async Task<IActionResult> Search(string searchTerm)
        {
            var viewModel = await GetPagedFlightsAsync(1, 5, null, searchTerm);
            return View("Index", viewModel);
        }
        public async Task<ActionResult> Index(int page = 1)
        {
            try
            {
                var viewModel = await GetPagedFlightsAsync(page);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync(ex.Message);
                return View();
            }
        }
    }
}
