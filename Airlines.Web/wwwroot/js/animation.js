document.addEventListener("DOMContentLoaded", () => {
    const tableSection = document.getElementById("table-section");
    const table = document.getElementById("table");
    const topButton = document.getElementById("go-to-top-button");

    topButton.addEventListener("click", () => {
        tableSection.scrollTo({
            behavior: "smooth",
            top: 0
        });
    });

    let tableOpacity = 0;

    const fadeIn = setInterval(() => {
        table.style.opacity = tableOpacity;
        tableOpacity += 0.01;
        
        if (tableOpacity === 1) {
            clearInterval(fadeIn);
        }
    }, 15);

    
    function checkTableHeight() {
        const tableHeight = table.scrollHeight;
        const sectionHeight = tableSection.clientHeight;
        
        if (tableHeight > sectionHeight) {
            topButton.style.display = 'block';
        } else {
            topButton.style.display = 'none';
        }
    }

    checkTableHeight();
    window.addEventListener('resize', checkTableHeight);
});