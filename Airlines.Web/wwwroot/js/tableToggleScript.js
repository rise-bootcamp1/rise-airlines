document.addEventListener('DOMContentLoaded', function () {
    const button = document.getElementById("toggle-table-button");
    button.addEventListener("click", function () {
        const tableSection = document.getElementById('table-section');
        const isHidden = tableSection.style.display === 'none';
        
        tableSection.style.display = isHidden ? 'block' : 'none';
        button.textContent = isHidden ? 'Show' : 'Hide';
    });
});

function checkItemsCount() {
    const table = document.getElementById("table");
    const tableElementsCount = table.rows.length;
    

    if (tableElementsCount > 3) {
        const button = document.createElement("button");
        
        button.id = "toggle-table-items-button";
        button.textContent = "Show only 3 items";
        button.className = "orange-button";
        button.onclick = function () {
            for (let i = 4; i < table.rows.length; i++) {
                table.rows[i].style.display = table.rows[i].style.display === "none" ? "" : "none";
            }
        };
        
        const pageContent = document.getElementById("table-controls");
        pageContent.appendChild(button);
    }
}

window.onload = function() {
    checkItemsCount();
};