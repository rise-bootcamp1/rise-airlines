document.addEventListener("DOMContentLoaded", () => {
    const name = document.getElementById("airline-name");
    const founded = document.getElementById("airline-founded");
    const fleetSize = document.getElementById("airline-fleet-size");
    const desc = document.getElementById("airline-description");
    
    const nameErrorSpan = document.getElementById("name-error");
    const foundedErrorSpan = document.getElementById("founded-error");
    const fleetSizeErrorSpan = document.getElementById("fleet-size-error");
    const descErrorSpan = document.getElementById("description-error");

    const submitButton = document.getElementById("submit-form-button");
    submitButton.disabled = false;

    const errors = {};

    name.addEventListener("blur", validateName);
    founded.addEventListener("blur", validateFounded);
    fleetSize.addEventListener("blur", validateFleetSize);
    desc.addEventListener("blur", validateDescription);

    function toggleSubmitButton() {
        submitButton.disabled = Object.values(errors).some(error => error);
    }

    function validateName() {
        const value = name.value.trim();
        let result = false;

        if (value === "") {
            errors.name = "Name is required";
        } else if (value.length < 1 || value.length > 10) {
            errors.name = "Name must be between 1 and 10 characteres";
        } else {
            errors.name = null;
            result = true;
        }
        nameErrorSpan.textContent = errors.name ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateFounded() {
        const value = founded.value.trim();
        let result = false;

        if (value === "") {
            errors.founded = "Founded is required";
        } else {
            errors.founded = null;
            result = true;
        }
        foundedErrorSpan.textContent = errors.founded ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateFleetSize() {
        const value = fleetSize.value.trim();
        let result = false;

        if (value === "") {
            errors.fleetSize = "Fleet size is required";
        } else if (value < 0 || isNaN(value)) {
            errors.fleetSize = "Fleet size must be a positive number";
        } else {
            errors.fleetSize = null;
            result = true;
        }
        fleetSizeErrorSpan.textContent = errors.fleetSize ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateDescription() {
        const value = desc.value.trim();
        let result = false;

        if (value === "") {
            errors.description = "Description is required";
        } else if (value.length < 1 || value.length > 500) {
            errors.description = "Description must be between 1 and 500 symbols.";
        } else {
            errors.description = null;
            result = true;
        }
        descErrorSpan.textContent = errors.description ?? "";

        toggleSubmitButton();
        return result;
    }
});