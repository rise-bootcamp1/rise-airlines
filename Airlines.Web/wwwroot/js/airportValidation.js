document.addEventListener("DOMContentLoaded", () => {
    const code = document.getElementById("airport-code");
    const name = document.getElementById("airport-name");
    const country = document.getElementById("airport-country");
    const city = document.getElementById("airport-city");
    const runways = document.getElementById("airport-runways");
    const founded = document.getElementById("airport-founded");

    const codeErrorSpan = document.getElementById("code-error");
    const nameErrorSpan = document.getElementById("name-error");
    const countryErrorSpan = document.getElementById("country-error");
    const cityErrorSpan = document.getElementById("city-error");
    const runwaysErrorSpan = document.getElementById("runways-error");
    const foundedErrorSpan = document.getElementById("founded-error");
    
    const submitButton = document.getElementById("submit-form-button");
    submitButton.disabled = false;

    const errors = {};

    code.addEventListener("blur", validateCode);
    name.addEventListener("blur", validateName);
    country.addEventListener("blur", validateCountry);
    city.addEventListener("blur", validateCity);
    runways.addEventListener("blur", validateRunways);
    founded.addEventListener("blur", validateFounded);

    function toggleSubmitButton() {
        submitButton.disabled = Object.values(errors).some(error => error);
    }

    function validateCode() {
        const value = code.value.trim();
        const pattern = /^[A-Za-z0-9]+$/;
        let result = false;

        if (value === "") {
            errors.code = "Airport Code is required";
        } else if (value.length < 1 || value.length > 3) {
            errors.code = "Airport code must be between 1 and 3 characters";
        } else if (!pattern.test(value)) {
            errors.code = "Airport code must be alphanumeric";
        } else {
            errors.code = null;
            result = true;
        }
        codeErrorSpan.textContent = errors.code ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateName() {
        const value = name.value.trim();
        let result = false;

        if (value === "") {
            errors.name = "Name is required";
        } else if (value.length < 1 || value.length > 50) {
            errors.name = "Name must be between 1 and 50 characters";
        } else {
            errors.name = null;
            nameErrorSpan.textContent = "";
            result = true;
        }
        nameErrorSpan.textContent = errors.name ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateCountry() {
        const value = country.value.trim();
        let result = false;

        if (value === "") {
            errors.country = "Country is required";
        } else if (value.length < 1 || value.length > 20) {
            errors.country = "Country must be between 1 and 20 characters";
        } else {
            errors.country = null;
            result = true;
        }
        countryErrorSpan.textContent = errors.country ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateCity() {
        const value = city.value.trim();
        let result = false;

        if (value === "") {
            errors.city = "City is required";
        } else if (value.length < 1 || value.length > 50) {
            errors.city = "City must be between 1 and 50 characters";
        } else {
            errors.city = null;
            result = true;
        }
        cityErrorSpan.textContent = errors.city ?? "";

        toggleSubmitButton();
        return result;
    }

    function validateRunways() {
        const value = runways.value.trim();
        let result = false;

        if (value === "") {
            errors.runways = "Runways count is required";
        } else if (value.length < 0 || isNaN(value)) {
            errors.runways = "Runways count must be a positive number";
        } else {
            errors.runways = null;
            result = true;
        }
        runwaysErrorSpan.textContent = errors.runways ?? "";
        
        toggleSubmitButton();
        return result;
    }

    function validateFounded() {
        const value = founded.value.trim();
        let result = false;

        if (value === "") {
            errors.founded = "Founded is required";
        } else {
            errors.founded = null;
            result = true;
        }
        foundedErrorSpan.textContent = errors.founded ?? "";
        
        toggleSubmitButton();
        return result;
    }
});