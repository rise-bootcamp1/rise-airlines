document.addEventListener("DOMContentLoaded", () => {
    const code = document.getElementById("flight-code");
    const departureAirport = document.getElementById("flight-departure-code");
    const arrivalAirport = document.getElementById("flight-arrival-code");
    const departureTime = document.getElementById("flight-departure-time");
    const arrivalTime = document.getElementById("flight-arrival-time");

    const codeErrorSpan = document.getElementById("code-error");
    const departureAirportErrorSpan = document.getElementById("departure-code-error");
    const arrivalAirportErrorSpan = document.getElementById("arrival-code-error");
    const departureTimeErrorSpan = document.getElementById("departure-time-error");
    const arrivalTimeErrorSpan = document.getElementById("arrival-time-error");
    
    const submitButton = document.getElementById("submit-form-button");
    submitButton.disabled = false;

    const errors = {};

    code.addEventListener("blur", validateCode);
    departureAirport.addEventListener("blur", () => validateAirport(departureAirport, departureAirportErrorSpan));
    arrivalAirport.addEventListener("blur", () => validateAirport(arrivalAirport, arrivalAirportErrorSpan));
    departureTime.addEventListener("blur", () => validateTime(departureTime, departureTimeErrorSpan));
    arrivalTime.addEventListener("blur", () => validateTime(arrivalTime, arrivalTimeErrorSpan));

    function toggleSubmitButton() {
        submitButton.disabled = Object.values(errors).some(error => error);
    }

    function validateCode() {
        const value = code.value.trim();
        const pattern = /^[A-Za-z0-9]+$/;
        let result = false;

        if (value === "") {
            errors.code = "Code is required";
        } else if (value.length < 1 || value.length > 5) {
            errors.code = "Flight code must be between 1 and 5 characters";
        } else if (!pattern.test(value)) {
            errors.code = "Flight code must be alphanumeric";
        } else {
            errors.code = null;
            result = true;
        }
        codeErrorSpan.textContent = errors.code ?? "";
        
        toggleSubmitButton();
        return result;
    }

    function validateAirport(airport, errorSpan) {
        const value = airport.value.trim();
        const pattern = /^[A-Za-z0-9]+$/;
        let result = false;

        if (value === "") {
            errors.airport = "Airport Code is required";
        } else if (value.length < 1 || value.length > 3) {
            errors.airport = "Airport code must be between 1 and 3 characters";
        } else if (!pattern.test(value)) {
            errors.airport = "Airport code must be alphanumeric";
        } else {
            errors.airport = null;
            result = true;
        }
        errorSpan.textContent = errors.airport ?? "";
        
        toggleSubmitButton();
        return result;
    }

    function validateTime(time, errorSpan) {
        const value = time.value.trim();
        let result = false;

        if (value === "") {
            errors.time = "Time is required";
        } else {
            errors.time = null;
            result = true;
        }
        errorSpan.textContent = errors.time ?? "";

        toggleSubmitButton();
        return result;
    }
});