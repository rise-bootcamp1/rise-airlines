CREATE TABLE [dbo].Airlines (
	Id INT CONSTRAINT PK_Airlines PRIMARY KEY IDENTITY,
	Name CHAR(10) UNIQUE NOT NULL,
	Founded DATE NOT NULL,
	FleetSize INT DEFAULT 0 NOT NULL,
	Description NVARCHAR(500) NULL,
);

CREATE INDEX IX_Founded ON [dbo].Airlines(Founded);
CREATE INDEX IX_FleetSize ON [dbo].Airlines(FleetSize);