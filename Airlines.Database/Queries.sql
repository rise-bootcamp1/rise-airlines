INSERT INTO [dbo].Airports (Name, City, Country, Code, Founded)
VALUES ('John F. Kennedy International Airport', 'New York', 'USA', 'JFK', GETDATE()),
	   ('Los Angeles International Airport', 'Los Angeles', 'USA', 'LAX',GETDATE()),
	   ('OHare International Airport', 'Chicago', 'USA', 'ORD', GETDATE())

INSERT INTO [dbo].Airports (Name, City, Country, Founded)
VALUES ('John F. Kennedy International Airport', 'New York', 'USA', GETDATE()),
	   ('Los Angeles International Airport', 'Los Angeles', 'USA', 'LAX',GETDATE()),
	   ('OHare International Airport', 'Chicago', 'USA', 'ORD', GETDATE())

INSERT INTO [dbo].Flights
VALUES ('FL123', 1, 2, DATEADD(minute, 1, DATEADD(day, 1, GETDATE())), DATEADD(hour, 1, DATEADD(day, 1, GETDATE()))),
	   ('FL101', 2, 3, DATEADD(minute, 1, DATEADD(day, 1, GETDATE())), DATEADD(hour, 2, DATEADD(day, 1, GETDATE())))
	   
INSERT INTO [dbo].Flights
VALUES ('FL003', 1, 2, DATEADD(minute, 1, GETDATE()), DATEADD(hour, 1, GETDATE())),
	   ('FL105', 2, 3, DATEADD(minute, 1, GETDATE()), DATEADD(hour, 2, GETDATE()))

SELECT Code as Airport_Identifier
FROM [dbo].Airports
WHERE Country = 'USA'

UPDATE [dbo].Flights
SET Arrival = 'ORD'
WHERE Code = 'FL123';

DELETE [dbo].Flights
WHERE Code = 'FL123';