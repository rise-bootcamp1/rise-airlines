DROP TABLE [dbo].Flights

CREATE TABLE [dbo].Flights (
	Id INT CONSTRAINT PK_Flights PRIMARY KEY IDENTITY,
	Code CHAR(5) UNIQUE NOT NULL,
	Departure INT NOT NULL,
	Arrival INT NOT NULL,
	DepartureDateTime DateTime NOT NULL,
	ArrivalDateTime DateTime NOT NULL,
	CONSTRAINT FK_Departure_Airport FOREIGN KEY (Departure) REFERENCES [dbo].Airports(Id),
	CONSTRAINT FK_Arrival_Airport FOREIGN KEY (Arrival) REFERENCES [dbo].Airports(Id),
	CONSTRAINT CHK_ArrivalAfterDeparture CHECK (DepartureDateTime < ArrivalDateTime),
	CONSTRAINT CHK_ArrivalDepartureFuture CHECK (DepartureDateTime > GETDATE() AND ArrivalDateTime > GETDATE())
);

CREATE INDEX IX_Departure ON [dbo].Flights (Departure);
CREATE INDEX IX_Arrival ON [dbo].Flights (Arrival);
CREATE INDEX IX_DepartureDateTime ON [dbo].Flights (DepartureDateTime);
CREATE INDEX IX_ArrivalDateTime ON [dbo].Flights (ArrivalDateTime);