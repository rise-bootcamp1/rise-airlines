INSERT INTO [dbo].Flights (Id, Departure, Arrival, DepartureDateTime, ArrivalDateTime)
VALUES ('FL004', 'JFK', 'LAX', GETDATE() + '3:00:00', GETDATE() + '5:00:00');

-- The following query will fail because the DepartureDateTime is greater than the ArrivalDateTime
INSERT INTO [dbo].Flights (Id, Departure, Arrival, DepartureDateTime, ArrivalDateTime)
VALUES ('FL002', 'JFK', 'LAX', '2024-04-13 12:00:00', '2024-04-13 10:00:00');

-- The following query will fail because the DepartureDateTime and  is in the past
INSERT INTO [dbo].Flights (Id, Departure, Arrival, DepartureDateTime, ArrivalDateTime)
VALUES ('FL003', 'JFK', 'LAX', '2022-04-13 10:00:00', '2022-04-13 12:00:00');

SELECT Code FROM [dbo].Flights;