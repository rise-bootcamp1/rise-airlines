SELECT 
    f.Id,
    f.DepartureDateTime as DepartureTime,
    f.ArrivalDateTime as ArrivalTime,
    depAirport.Name AS DepartureAirport,
    arrAirport.Name AS ArrivalAirport
FROM 
    [dbo].Flights f
JOIN 
    [dbo].Airports depAirport ON f.Departure = depAirport.Id
JOIN 
    [dbo].Airports arrAirport ON f.Arrival = arrAirport.Id
WHERE 
    CAST(f.DepartureDateTime AS DATE) = CAST(GETDATE() + 1 AS DATE);