DROP TABLE [dbo].Airports

CREATE TABLE [dbo].Airports (
	Id INT CONSTRAINT PK_Airports PRIMARY KEY IDENTITY,
	Code CHAR(3) UNIQUE NOT NULL,
	Name NVARCHAR(50) NULL,
	Country NVARCHAR(20) NULL,
	City NVARCHAR(50) NULL,
	Runways_Count INT DEFAULT 0,
	Founded DATE NOT NULL
);

CREATE INDEX IX_Country ON [dbo].Airports (Country);
CREATE INDEX IX_City ON [dbo].Airports (City);