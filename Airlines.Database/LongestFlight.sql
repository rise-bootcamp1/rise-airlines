SELECT Code, Departure, DepartureDateTime, Arrival, ArrivalDateTime
FROM [dbo].Flights
WHERE DATEDIFF(minute, DepartureDateTime, ArrivalDateTime) = (
    SELECT MAX(DATEDIFF(minute, DepartureDateTime, ArrivalDateTime)) 
    FROM [dbo].Flights
);